<?php

namespace App\Http\Controllers\Imobi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Imoveis;
use Auth;
use DB;

class ImoveisController extends Controller
{
    public function index()
    {
		$usuario_logado = Auth::user();
		$imoveis = Imoveis::all();

		
		return redirect()->back()->with(['usuario' => $usuario_logado, 'imoveis'=>$imoveis]); //return view('Imobi.home.home')->with(['usuario' => $usuario_logado, 'imoveis'=>$imoveis]);
    }

    public function cadastrar(Request $request){
    	$data = $request->all();

    	$cad['titulo'] = $data['titulo'];
		$cad['valor'] = $data['valor'];
		$cad['texto'] = $data['texto'];
		$set = Imoveis::create($cad);
		$imoveis = Imoveis::all();
		
		return view('Imobi.home.home')->with(['sucesso'=>true, 'imoveis'=>$imoveis ]);
    }

    public function create(){
		return view('imobi.home.create');
	}
	
	public function store(Request $request){

		$validator = Validator::make($request->all(), [
			'unid_fed' => 'required|max:2',
			'estado' => 'required|unique:estados',
		]);

		if ($validator->fails()) {
			return Redirect::to('uf/create')
				->withErrors($validator)
				->withInput();
		} else {
			
			$data = $request->all();
			$estado['unid_fed'] = $data['unid_fed'];
			$estado['estado'] = $data['estado'];

			Estados::create($estado);
			// redirect
			return Redirect::to('home')->with(['sucesso'=>true]);
		}
	}
	
	public function view($id){
		
		$imovel = Imoveis::find($id);
		
		return view('Imobi.home.view')->with(['imovel' => $imovel]);
		
	}

    public function edit(Request $request){

	    $data = $request->all();
        $imovel = Imoveis::find($data['id']);

        $cad['titulo'] = $data['titulo'];
        $cad['valor'] = $data['valor'];
		$cad['texto'] = $data['texto'];
		
        $imovel->update($cad);
		$imoveis = Imoveis::all();
		
        return view('Imobi.home.home')->with(['sucesso'=>true,'imoveis'=>$imoveis]);
    }

   	public function deletar($id){		
        $imovel = Imoveis::find($id);
        $imovel->delete();
		$imoveis = Imoveis::all();
        return view('Imobi.home.home')->with(['deletado'=>true,'imoveis'=>$imoveis]);
	}
	
	public function dash(){
		$imoveis = Imoveis::orderBy('id','desc')->take(3)->get();
		$textos = Imoveis::orderBy('id','desc')->take(1)->get();
		
		return view('Imobi.dash.home')->with(['imoveis'=>$imoveis,'textos'=>$textos]);
	}

}
