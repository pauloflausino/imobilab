<html>
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
                <title>Sistema | Imobilab</title>
        
        <!-- Bootstrap -->
        <link href="{{ url('Imobilab/assets/css/bootstrap.min.css') }}" rel="stylesheet">
        
        <style>
            body {
                background: url('{{ url("Imobi/assets/img/bg-login.png") }}') no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                z-index:-1;
            }

            .img-logo-top {
                padding-top:64px;
            }

            #inputEmail {
                outline: none;
            }

            #inputPassword {
                outline: none;
            }

            .control-group {
                padding-bottom:16px;
            }

            a {
                color: #d9e1eb;
                font-size: 12px;
                font-weight: 100;
                text-decoration:none;
            }

            a:hover {
                color: #d9e1eb;
                text-decoration:none;
            }

            a:active {
                color: #d9e1eb;
                text-decoration:none;
            }
        </style>
    </head>
		<body style="min-width: 100%; min-height: 100%; margin: 0; padding: 0;">

        <div class="bk-home"></div>
        <div class="container-fluid text-center" style="margin-top: 60px;">
            <div class="col-md-4 col-lg-4">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <p class="img-logo-top" align="center"><h1 align='center' style='color:white'>LOGIN</h1></p>
                <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10" style="margin-left:33px;border: solid 1px rgba(255,255,255,0.1); background-color: rgba(255,255,255,0.1); padding: 10px 40px 10px 40px;-webkit-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.21);-moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.21);box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.21);">
                                        <div style="width:100%; color:white; display:block; margin:0 auto; margin-bottom:20px; margin-top:20px; text-align:center;">
                                            </div>
                                        <form class="form-horizontal" name="form_login" method="POST" action="{{ url('login') }}">
											{{ csrf_field() }}
                        <div class="control-group">
                            <div class="controls">
                                <input id="inputEmail" autocomplete="off" name="email" placeholder="Digite o seu e-mail..." style="width: 100%; color: #d9e1eb; text-align: center; border: 0; background-color: transparent;" type="email" required >
                            </div>
                            
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <input id="inputPassword" autocomplete="off" name="password" placeholder="Digite a sua senha..." style="width: 100%; color: #00c6d7; text-align: center; border: 0; background-color: transparent;" type="password" required>
                            </div>
                            
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <button class="btn" type="submit" style="width: 100%;height:40px; color: #fff; background-color: #00c6d7;">ENTRAR</button>

								 

                            </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11" style="margin-left:5px;margin-top:15px;">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="padding-left: 0; margin-top: 15px;">
                        <img src="{{ url('Imobi/assets/img/imobilab.png') }}" style="width: 170px; float: left; padding-left: 10px;">
                    </div>
                    
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
            </div>
        </div>
   
</body>
</html>