@extends('PMTS._layout.senha_header_footer')


@section('head')
<title>Recuperar password</title>
<meta name='description' content='Recuperar password'>
<meta name='keywords' content='palabras, clave'>
<meta name='robots' content='noindex,nofollow'>
@stop

@section('content')

<h1 align="center">Esqueci Senha:</h1>
<br><br><br>
{{Session::get("message")}}

@if (count($errors) > 0)
                    <div class="alert alert-danger" style="position:absolute;top:5%;width:100%">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

@if (Session::has('sucesso'))
                    <div class="alert alert-success" style="align:center;position: absolute;top:5%;width:100% ">
                       Enviado com sucesso! Verifique seu e-mail.
                    </div>
                @endif
			
			
	
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"  style="align:center;position: absolute;left: 32%; top: 35% ">
			<div class="col-xs-11 col-sm-11 col-md-11 col-lg-10" style="margin-left:33px;border: solid 1px rgba(255,255,255,0.1); background-color: rgba(255,255,255,0.1); padding: 10px 40px 10px 40px;-webkit-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.21);-moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.21);box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.21);" >
				<div style="width:100%; color:white; display:block; margin:0 auto; margin-bottom:20px; margin-top:20px; text-align:center;"> </div>
			
				<form method="POST" action='redefinir'>
					{{ csrf_field() }}
					<div class="control-group">
						<div class="controls">
							<label >E-Mail *</label>
							<div >
								<input type="email" class="form-control" name="email" value="" required>
							</div>
						</div>
					</div><br>
					<div >
						<button type="submit" class="btn btn-primary">
							Recuperar Senha
						</button>
					</div>
				</form>
			</div>
			
		</div>
	


@stop




