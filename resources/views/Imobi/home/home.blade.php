@extends('Imobi._layout.header_footer')

@section('title', 'Home')



<!-- ************************************* -->
<!-- SCRIPTS -->
<!-- ************************************* -->
@section('css')
    <link href="{{ url('Imobi/assets/plugins/datatable/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
@endsection
<!-- ************************************* -->
@section('plugins')
    <script src="{{ url('Imobi/assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('Imobi/assets/plugins/datatable/js/dataTables.bootstrap.min.js') }}"></script>
@endsection
<!-- ************************************* -->
@section('scripts')

<script>
	function link_rapido(){
		$('#ExibirLinksHome').modal('show');
	};
	
	 $(document).on('click', '#dash', function() {
            window.location.href = '{{url('/imoveis/dash')}}';
        });
	
</script>


@endsection
<!-- ************************************* -->
<!-- FIM SCRIPTS -->
<!-- ************************************* -->

@section('content')


<style>
    .fundoInicio {
        background: #fff;
    }

    .col-xs-12.fundoInicio {
        background-size: 800px;
    }

    .col-sm-12.fundoInicio {
        background-size: 1000px;
    }

    .col-md-12.fundoInicio {
        background-size: 1200px;
    }

    .col-lg-12.fundoInicio {
        background-size: 1350px;
    }

    .btnEfeto,
    .btnEfeto:hover {
        cursor: pointer;
        color: #fff;
    }

    .btnHome{
        width: 25%;
        height: 110px;
        float: left;
        max-width: 110px;
    }

    .btnHome small{
        float: left;
        padding-top: 70px;
        width: 100%;
        text-align: center;
        font-size: 9px;
        font-weight: bold;
    }

    .btnCadastroSite{
        background: url({{ url('Imobi/assets/img/cadastroSite.png') }}) #52c6d9 no-repeat center 20px;
        background-size: 60px;
    }

    .btnCadastroIntervencao{
        background: url({{ url('Imobi/assets/img/cadastroIntervencao.png') }}) #003347 no-repeat center 15px;
        background-size: 40px;
    }

    .btnLink{
        background: url({{ url('Imobi/assets/img/link.png') }}) #e5663d no-repeat center 20px;
        background-size: 40px;
    }

    .btnPesquisaSite{
        background: url({{ url('Imobi/assets/img/pesquisaSite.png') }})  #2bb673 no-repeat center 20px;
        background-size: 40px;
    }

    .btnChecklist{
        background: url({{ url('Imobi/assets/img/checklist.png') }}) #93268f no-repeat center 20px;
        background-size: 40px;
    }

    .btnPesquisaIntervencao{
        background: url({{ url('Imobi/assets/img/pesquisaIntervencao.png') }})  #ede83b no-repeat center 20px;
        background-size: 40px;
    }

    .contentApp {
        padding-left: 0;
        padding-right: 0;
    }
	
	
</style>	



    

    <!-- Modal de cadastro de Imóveis -->
    <div id="novoSite" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Formulário Imobiliário</h4>
                </div>
                <form action="{{ url('imoveis/cadastrar') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <h4 class="col-md-offset-2" style="color: #007486;">Imóveis</h4>
                            <div class="elementGroup">
                                <div class="form-group row">
                                    <div class="col-md-4"><label style="float: right;">* Título</label></div>
                                        <div class="col-md-6"><input type="text" class="form-control" name="titulo" value="{{ old('id_sequencial') }}" required></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4"><label style="float: right;">* Valor</label></div>
                                        <div class="col-md-6"><input type="text" class="form-control" name="valor" value="{{ old('valor') }}" required></div>
                                    </div>
									<div class="form-group row">
                                        <div class="col-md-4"><label style="float: right;">Texto</label></div>
                                        <div class="col-md-6"><input type="text" class="form-control" name="texto" value="{{ old('texto') }}" ></div>
                                    </div>
                            </div>
                            <div class="modal-footer">
                               <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                               <button type="submit" class="btn btn-primary">Cadastrar</button>
                            </div>
                    </div>    
                </form>
            </div>
        </div>
    </div>
                <!-- FIM Modal de cadastro de site -->

@if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            </div>
			@if (Session::has('sucesso'))
				<div class="alert alert-success">
					Cadastrado com sucesso!
				</div>
			@endif
             <div class="panel-heading" style="height: 50px;">
                        <h3 class="panel-title" style="margin-top: 6px;">Gestão de Números</h3>
                        <button class="btn btn-primary" style="float: right; margin-top: -25px;" data-toggle="modal" data-target="#novoSite">NOVO NÚMERO +</button>
						<button class="btn btn-primary" style="float: right; margin-top: -25px;margin-right: 20px;" data-toggle="modal" data-target="#dash" id='dash'>VER DASHBOARD</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                    </div>
			<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 fundoInicio' style='padding-bottom: 10px;margin-bottom:15px;'>
                 <br>
                 @if(!empty($imoveis))
                <table id="lista_imovel" class="table table-striped table-bordered nowrap dataTablePTBR" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>TÍTULO</th>
                                <th>VALOR</th>
                                <th>DATA/HORA</th>
                                <th colspan="2">AÇÕES</th>
                            </tr>
                            </thead>
                            <tbody id="sitesList">
                                @foreach($imoveis as $imovel)
                                <tr>
                                    <td>{{$imovel->id }} </td>
                                    <td>{{$imovel->titulo }} </td>
                                    <td>{{$imovel->valor }} </td>
                                    <td>{{$imovel->created_at }} </td>
                                        
                                    <td><a href="{{ URL::to('imoveis/view/') }}/<?php echo $imovel->id?>" > EDITAR [-] </a></td>
									
                                    <td><a href="{{ URL::to('imoveis/deletar/') }}/<?php echo $imovel->id?>" onclick="return confirm('DESEJA REALMENTE EXCLUIR ESSE REGISTRO?')"> EXCLUIR [X] </a></td>                                    
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                @endif
				@if (Session::has('sucesso'))
				<div class="alert alert-success">
					Cadastrado com sucesso!
				</div>
			@endif
                <div class='col-md-1 col-lg-1'>
                </div>
            </div>			
        </div>
    </div>
@endsection