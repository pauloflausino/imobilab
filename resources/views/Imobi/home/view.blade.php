@extends('Imobi._layout.header_footer')
@section('title', 'Estados')

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('sucesso'))
        <div class="alert alert-success">
            Cadastrado com sucesso!
        </div>
    @endif

    <div class="container-fluid ">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="height: 50px;">
                        <h3 class="panel-title" style="margin-top: 6px;">Estados</h3>
                    </div>
                    <div class="panel-body">
                        <form action="{{ url('imoveis/edit') }}" method="POST">
                            {{ csrf_field() }}                            
                            <div class="modal-body">
                                <div class="elementGroup">
                                    <div class="form-group row">
                                        <div class="col-md-4"><label style="float: right;">* Imóvel</label></div>
                                        <div class="col-md-6">
                                            <input type="hidden" name="id" value="{{$imovel->id}}">
                                            <input type="text" value="{{$imovel->titulo}}" name="titulo" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4"><label style="float: right;">* Valor</label></div>
                                        <div class="col-md-6">
                                            <input type="text" value="{{$imovel->valor}}" name="valor" class="form-control" required>
                                        </div>
                                    </div>
									<div class="form-group row">
                                        <div class="col-md-4"><label style="float: right;">Texto</label></div>
                                        <div class="col-md-6"><input type="text" class="form-control" name="texto" value="{{ old('texto') }}" ></div>
                                    </div>
                                </div>
                            </div>                            
                            <a class="btn btn-default" href="{{ URL::to('home') }}">Voltar</a>
                            <button type="submit" class="btn btn-primary">Atualizar</button>                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection