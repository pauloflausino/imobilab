@extends('PMTS._layout.header_footer')
@section('title', 'Dashboard')

@section('content')
<style type="text/css">
    .custom-alert {
    padding: 2rem 1.25rem;
    margin-bottom: 2rem;
    border: 1px solid transparent;
    border-radius: .25rem;
    background-color: #cbffcc;
    border-color: #a2ffbc;
    color: #676767;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
    transition: 0.2s;
}

.custom-alert:hover {
    box-shadow: 0 6px 10px 0 rgba(0,0,0,0.14), 0 1px 18px 0 rgba(0,0,0,0.12), 0 3px 5px -1px rgba(0,0,0,0.3);
}

.custom-alert a {
    text-decoration:none;
}

.custom-alert a:hover {
    color:#000;
}

.myalert {
    padding:40px 0px;
}

.alert-dismissible.custom-alert .close {
    position: relative;
    top: -.75rem;
    right: -1.25rem;
    padding: .75rem 1.25rem;
    color: inherit;
}

.green-alert {
    background-color: #dff0d8;
    border-color: #d0e9c6;
    color: #3c763d;
    border-bottom: 2px solid;
}

.red-alert {
    background-color: #f2dede;
    border-color: #ebcccc;
    color: #a94442;
    border-bottom: 2px solid;
}

.info-alert {
    background-color: #d9edf7;
    border-color: #bcdff1;
    color: #31708f;
    border-bottom: 2px solid;
}

.warning-alert {
    background-color: #fcf8e3;
    border-color: #faf2cc;
    color: #8a6d3b;
    border-bottom: 2px solid;
}
</style>

    <div class="container-fluid ">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="margin-top: 6px;">Dashboard</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="alert alert-dismissible show custom-alert info-alert" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong>Olá {{$user->name}}!</strong> <a href="{{ url('/todo') }}" class="alert-link">{{$count > 1?'Existem':'Existe'}} {{$count}} {{$count > 1?'atividades aguardando você':'atividade aguardando você'}}</a>, para visualizar clique no link.
                            </div>
                        </div>
                        <div class="col-md-12 table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>ID PMTS</th>                                    
                                    <th>ID VIVO</th>                                    
                                    <th>Identificação</th>
                                    <th>Tipo Intervenção</th>
                                    <th>Programa</th>
                                    <th>Tecnologia</th>
                                    <th>Regional</th>                                  
                                </tr>
                                </thead>
                                <tbody>                                
                                    @foreach($intervencoes as $intervencao)                                        
                                        <tr> 
                                            <td><a href="{{ url('/intervencao/detalhes/'.$intervencao->id) }}" target="_blank">{{ $intervencao->id_pmts }}</a></td>                                            
                                            <td>{{$intervencao->id_vivo}}</td>                                            
                                            <td>{{$intervencao->identificacao}}</td>
                                            <td>{{$tipos_programas[$intervencao->tipo_programa]}}</td>
                                            <td>{{$intervencao->programa}}</td>
                                            <td>{{$intervencao->tecnologia}}</td>                                            
                                            <td>{{$intervencao->regional}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
@endsection