<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- FAVICON -->
	<link rel="shortcut icon" href={{ url('Imobi/assets/img/favicon.ico') }}

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Imobi | Imoveis </title>

    <!-- Bootstrap -->
    <link href="{{ url('Imobi/assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="{{ url('Imobi/assets/css/skin.css') }}" rel="stylesheet"></link>
	<style>
		.hidetelefone{
			display: none;
		}
		.hidelinks{
			display: none;
		}
		.tituloSubMenu h4 {
			color: #00c6d7;
			border-bottom: solid 2px #00c6d7;
			font-size: 12px;
			padding-bottom: 15px;
			min-height: 30px;
		}
		.tituloSubMenu a {
			color: #fff;
			float: left;
			width: 100%;
			padding: 8px;
			font-size: 11px;
			text-decoration: none !important;
		}

		.hidemenu{
			display: none;
		}
		.linkMenu2{
			cursor: pointer;
		}
		.elemento{
			    width: 103%;
				height: 100vh;
				padding: 15px;
				margin-left: -15px;
		}
		#MenuFull{
			background: #fff;
			position: fixed;
			width: 100%;
			height: 100vh;
			top: 0;
			z-index: 999999;
		}
		#mBar{
			height: 100vh; 
			padding: 0;
			color: #696969;
		}
		#mBar > ul{
			list-style-type: none;
			margin: 0;
			padding: 0;
		}
		#mBar > ul > li{
			padding: 12px;
			padding-left: 25px;
		}
		#mBar > ul > li:hover{
			padding: 12px;
			padding-left: 25px;
			background-color: #003245;
			color: #fff;
		}

				
		.lielem > a:link, a:visited{
			text-decoration: none;
		}
		
		.tituloMenu {
			cursor: pointer;
			color: #696969;
			text-decoration: none !important;
		}
		.tituloMenu:hover{
			color: #fff;
		}
		.actionMenu{
			background-color: #003245;
			color: #fff;
		}
		
		aside .aside-secs-wrapper .aside-menu-option,
aside .aside-secs-wrapper .aside-back,
.sidebar .first-level  {
  width: 100%;
}

aside .aside-secs-wrapper .sec-content-wrapper {
  width: 300px;
}


.sidebar {
  border:none;
}

@media all {
  body {
    min-width: 100rem;
    min-width: 100vw;
  }

  .container_12 {
    width: 100rem;
    width: 100vw;
  }

  .logo-wrapper .grid_8 {
    width: 55rem;
    width: 55vw;
    margin: 0 1rem;
    margin: 0 1vw;
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
  }

  .logo-wrapper .message-header {
    max-width: 230px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }

  .logo-wrapper .grid_4 {
    float: right;
    width: 40rem;
    width: 40vw;
    margin: 0 2rem 0 1rem;
    margin: 0 2vw 0 1vw;
  }

  .sidebar {
    display: none;
    -moz-opacity: 0;
    opacity: 0;
    filter: alpha(opacity=0);
    visibility: hidden;
    position: fixed;
    top: 0;
    right: -260px;
    z-index: 999;
    margin: 0;
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
  }

  .sidebar.active {
    /*right: -282px;*/
    right: -220px;
    -moz-opacity: 1;
    opacity: 1;
    filter: alpha(opacity=100);
    visibility: visible;
    display: block;
    /*
    -webkit-box-shadow: -1px 0 5px rgba(0,0,0,0.3);
    -moz-box-shadow: -1px 0 5px rgba(0,0,0,0.3);
    box-shadow: -1px 0 5px rgba(0,0,0,0.3);
    */
  }

  aside {
    max-height: 100rem;
    max-height: 100vh;
    height: 100rem;
    height: 100vh;
  }

  .h-btn {
  display: block;
  padding: 17px;
  height: 54px;
  width: 55px;
  text-indent: -99999px;
  position: relative;
  margin: 0 -1px;
  border-left: 1px solid transparent;
  border-right: 1px solid transparent;
}

.h-btn.push {
  display: none;
}

.h-btn:hover {
  background-color: #fff;
  border-left-color: #d5d5d5;
  border-right-color: #d5d5d5;
}

.h-btn:before {
  content: "";
  display: block;
  height: 20px;
  width: 20px;
  background: url({{ url('Imobi/assets/img/sprite.png') }}) no-repeat;
  float: left;
  -webkit-transition: all 0.3s ease-in;
  -moz-transition: all 0.3s ease-in;
  transition: all 0.3s ease-in;
}

.h-off:before {
  background-position: -748px -1px;
}

.h-off:hover:before {
  background-position: -748px -24px;
}

.push:before {
  background-position: -727px -1px;
}

.push:hover:before {
  background-position: -727px -24px;
}


  .mp-pusher.active > .push-deactivator {
    visibility: visible;
    opacity: 1;
  }

  .main-container .area-content {
    width: 770px;
    margin: 0 auto;
    display: block;
    float: none;
  }

  .mp-pusher.active {
    -webkit-transform: translate3d(-220px, 0, 0)!important;
    -moz-transform: translate3d(-220px, 0, 0)!important;
    transform: translate3d(-220px, 0, 0)!important;
  }

  .scroller {
    /*width: 1014px;*/
    overflow-y: auto;
    /*overflow-x: auto;*/
  }

  .scroller-inner {
    /*width: 1014px;*/
  }

.h-btn2 {
  display: block;
  padding: 17px;
  height: 54px;
  width: 55px;
  text-indent: -99999px;
  position: relative;
  margin: 0 -1px;
  border-left: 1px solid transparent;
  border-right: 1px solid transparent;
}

.h-btn2.push {
  display: none;
}

.h-btn2:hover {
  background-color: #fff;
  border-left-color: #d5d5d5;
  border-right-color: #d5d5d5;
}

.h-btn2:before {
  content: "";
  display: block;
  height: 20px;
  width: 20px;
  background: url({{ url('Imobi/assets/img/sprite.png') }}) no-repeat;
  float: left;
  -webkit-transition: all 0.3s ease-in;
  -moz-transition: all 0.3s ease-in;
  transition: all 0.3s ease-in;
}

.h-off2:before {
  background-position: -726px -1px;
}

.h-off2:hover:before {
  background-position: -726px -24px;
}

.push2:before {
  background-position: -727px -1px;
}

.push2:hover:before {
  background-position: -727px -24px;
}

 
		


ul{
	list-style: none;
	
}

.esconder{
			display: none;
		}

#preloader {
    position: absolute;
    left: 0px;
    right: 0px;
    bottom: 0px;
    top: 0px;
    background: #ccc;
}



.s-sism.success {
    background-position: -31px 0px;
}

.s-sism.caution {
  background-position: -81px 0px;
}

.s-sism.cancel {
  background-position: -56px 0px;
}

.s-sism {
    display: block;
    height: 24px;
    width: 24px;
    background: url({{ url('Imobi/assets/img/sprite.png') }}) no-repeat;
    text-indent: -99999px;
    margin: 15px;
}

.grid_1,
.grid_2,
.grid_3,
.grid_4,
.grid_5,
.grid_6,
.grid_7,
.grid_8,
.grid_9,
.grid_10,
.grid_11,
.grid_12 {
  display: inline;
  float: left;
  margin-left: 10px;
  margin-right: 10px;
}
.push_1, .pull_1,
.push_2, .pull_2,
.push_3, .pull_3,
.push_4, .pull_4,
.push_5, .pull_5,
.push_6, .pull_6,
.push_7, .pull_7,
.push_8, .pull_8,
.push_9, .pull_9,
.push_10, .pull_10,
.push_11, .pull_11 {
  position: relative;
}
.container_12 .grid_4 {
  width: 300px;
}

.icones_topo{
	width: 370px;
	float: right;
	margin-right:-275px;
}

ul.icones li{
	display: inline;
	float: left;
	margin: 0 0px;
	pading: 0px;
	border-left: 1px solid transparent;
	border-right: 1px solid #ddd;
	
}

		
	</style>
    @yield('css')

</head>


<body  style="background-color: #f1f3fa;overflow-x:hidden;overflow-y: scroll;" >
	
		<div class="icones_topo"  align='right'> 
			<ul class="icones">				
				<li>
					<a class="h-btn h-off close-that-nav tipsy-n" href="{{ route('logout') }}" title="Sair do ambiente" alt="Sair"></a>
					
				</li>								
				
			</ul>
		</div><!-- end .grid_4 -->




<!-- MENU HEADER -->
<div class="container-fluid" style="padding-left: 0; padding-right: 0; margin-bottom: 30px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0; padding-right: 0;">
        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="background-color: #003347; padding-left: 0; padding-right: 0;"  >
            <a href="#" class="linkMenu" style="color:#fff;" data-toggle="modal" data-target="#modalMenu">
                <div style="height: 60px;background: url({{ url('Imobi/assets/img/imobilab.png') }}) no-repeat 25px center; background-size: 17px;">
                <img src="{{ url('Imobi/assets/img/imobilab.png') }}" style="width: 170px; padding-top: 18px; padding-right: 30px;">
                    
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11" style="height: 60px; background: url({{ url('Imobi/assets/img/read.png') }}) #003045 no-repeat 0 0; padding-left: 0; padding-right: 0;">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="height: 60px; background: url({{ url('Imobi/assets/img/Imobi.png') }}) no-repeat 50px center; background-size: 100px;"></div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right" style="height: 80px;">
                
            </div>
        </div>
    </div>
</div>

<script>
	
	window.onload = function(e){ 
		$('#mBar > ul > li').hover(function (){
		});
		$('#carregando').removeClass('esconder');
		$( document ).ready(function() {
			$('#carregado').removeClass('esconder');
			$('#carregando').addClass('esconder');
		});	
	}	
	
	
	$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
	
	
</script>

@if (Session::has('permissao'))
	<div class="alert alert-success">
		Você não tem permissão para acessar aquele local.
	</div>
@endif


<div id='MenuFull' class="hidemenu">
	<div class="container-fluid">
		<div class="row">
			<div id="mBar" class="col-md-2">
				<ul>
					<li style="background: #fff; color: #000;" class="linkMenu2">
						<h4 class="modal-title" style="font-size: 11px; font-weight: bold; color: #00c6d7;">
							<span style="font-size: 13px;" class="fecharModalMenu" aria-hidden="true">×</span>
							<span class="fecharModalMenu"> MENU</span>
						</h4>
					</li>
					<li style="background: #fff; padding-bottom: 20px;"><img style="height: 20px;" src="{{ url('Imobi/assets/img/pmts-azul.png') }}"></li>
					<li class="lielem" alt="d5"><a class="tituloMenu" href="{{url('/home')}}">Inicio </a></li>
					<li class="lielem" alt="d5"><a class="tituloMenu" href="{{url('/home/dashboard')}}">Dashboard </a>
					<li class="lielem" alt="d2">Pesquisar</li>
					<li class="lielem" alt="d3">Cadastro</li>
					<li class="lielem" alt="d4">Configurações</li>
					<li class="lielem" alt="d6"><a href='javascript:retornaLinksRapidos();' style='padding-top: 10px; padding-bottom: 10px;' class='tituloMenu opt-links-rapidos' alt="Inicio">Links Rápidos</a></li>
					<li class="lielem" alt="d7"><a href='javascript:retornaTelefone();' style='padding-top: 10px; padding-bottom: 10px;' class='tituloMenu opt-telefones-uteis' alt="Inicio">Telefones Úteis</a></li>
					<li class="lielem" alt="d5"><a class="tituloMenu" href="{{url('/users/alterarsenha')}}/{{Auth::user()->id}}">Alterar Senha </a></li>
					
				</ul>
			</div>
			<div class="col-md-10">
				<div id="d1" style="display: none; background: #003245; color: #fff;" class="elemento">
					
				</div>
				<div id="d2" style="display: none; background: #003245; color: #fff;" class="elemento">
					<div id="containerPesquisas" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 conteudoMenu menuArea conteudoActiveMenu" style="height: 412px;">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0; padding-right: 0; padding-bottom: 40px;">
							<img src="{{ url('Imobi/assets/img/telefonica.png') }}" style="float: right; width: 190px; margin-top: 10px;">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
							<h4>Pesquisa</h4>
							<a href="{{ url('/sites') }}">Site</a>
							<a href="{{ url('/intervencoes') }}">Intervenção</a>
							<a href="{{ url('/todo') }}">To Do</a>
							
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
						</div>
					</div>
				</div>
				<div id="d3" style="display: none; background: #003245; color: #fff;" class="elemento">
					<div id="containerPesquisas" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 conteudoMenu menuArea conteudoActiveMenu" style="height: 412px;">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0; padding-right: 0; padding-bottom: 40px;">
							<img src="{{ url('Imobi/assets/img/telefonica.png') }}" style="float: right; width: 190px; margin-top: 10px;">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
							<h4>Cadastro</h4>
							<a href="{{ url('/sites') }}">Site</a>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
						</div>
					</div>
				</div>
				<div id="d4" style="display: none; background: #003245; color: #fff;" class="elemento">
					<div id="containerPesquisas" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 conteudoMenu menuArea conteudoActiveMenu" style="height: 412px;">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0; padding-right: 0; padding-bottom: 40px;">
							<img src="{{ url('Imobi/assets/img/telefonica.png') }}" style="float: right; width: 190px; margin-top: 10px;">
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
									<h4>USUÁRIO</h4>
									<a href="{{ url('/users') }}">Usuários</a>
								</div>								
								<?php if(Auth::user()->permissao  == 1){?>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
										<h4>SITE</h4>
										<a href="{{ url('/uf') }}">UF</a>
										<a href="{{ url('/regionais') }}">Regional</a>
										<a href="{{ url('/municipios') }}">Municípios</a>
										<a href="{{ url('/distritos') }}">Distrito</a>
										<a href="{{ url('/ddd') }}">DDD</a>
										<a href="{{ url('/modalidades') }}">Modalidade</a>
										<a href="{{ url('/detentoras') }}">Detentora</a>
										<a href="{{ url('/tipos_site') }}">Tipo Site</a>
									<a href="{{ url('/tipos_infra') }}">Tipo Infra</a>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
										<h4>INTERVENÇÃO</h4>
										<a href="{{ url('/programas') }}">Programa</a>
										<a href="{{ url('/programas_areas') }}">Programa Área</a>
										<a href="{{ url('/tecnologias') }}">Tecnologia</a>
										<a href="{{ url('/tipos_intervencoes') }}">Tipo intervenção</a>
										<a href="{{ url('/classificacoes') }}">Classificação</a>
										<a href="#">Tipo SCI</a>
										<a href="#">Fornecedor Aquisição</a>
										<a href="#">Fornecedor Infra/Equipamento</a>
										<a href="#">Categoria Equipamento</a>
										<a href="#">Frequência RF</a>
										<a href="#">Frequência Transmissão</a>
										<a href="#">Diâmetro Antena</a>																		
									</div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
										<h4>PROPRIEDADE</h4>
										<a href="#">Projeto</a>
										<a href="#">Transmissão</a>
										<a href="#">Regulatório</a>
										<a href="#">Marketing Nacional</a>
										<a href="#">Marketing Regional</a>																	
										<a href="#">Ano</a>
										<a href="#">Mega</a>
										<a href="#">Nome</a>
										<a href="#">Master</a>
									</div>								
								<?php }?>								
							</div>
						</div>
						<?php if(Auth::user()->permissao  != 1){?>
							<br /><br /><br />	<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
							<br /><br /><br />
						<?php }?>
						<div class="col-md-12">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu" style="margin-top: -132px">
									<h4>ATIVIDADE</h4>
									<a href="{{ url('/atividades') }}">Atividades</a>
									<a href="{{ url('/diretrizes') }}">Diretrizes</a>
									<a href="{{ url('/regras') }}">Regras Site</a>
									<a href="{{ url('/regrasintervencao') }}">Regras Intervenção</a>
								</div>
							</div>
						</div>						
						<div class="col-md-12">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu">
									
								</div>
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tituloSubMenu" style="margin-top: -131px">
									<h4>OUTROS</h4>
									<a href="{{ url('/suporte') }}">Suporte</a>
									<?php if(Auth::user()->permissao  == 1){?>
										<a href="{{ url('/pep') }}">Tipo PEP</a>										
									<?php }?>									
								</div>								
							</div>
						</div>
						
					</div>
				</div>
				<div id="d6" style="display: none; background: #003245; color: #fff;" class="elemento">
					<!-- Modal de Links Rápidos -->
					<div id="ExibirLinks" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<?php if(Auth::user()->permissao  == 1){?>
									<button id="NovoTelefone" class="btn btn-primary" style="float: right;" data-toggle="modal" onclick="javascript:CadastraLink();" >Novo Link</button>
									<?php } ?>
									<h4 class="modal-title" id="myModalLabel" style="color:black">Links Rápidos</h4>
								</div>
					
								<table id="links_rapidos" class="table table-striped table-bordered nowrap dataTablePTBR" cellspacing="0" width="100%" >
									<thead>
										<tr>
											<th style="color:black">NOME</th>
											<th style="color:black">URL</th>
											<th style="color:black">AÇÃO</th>
											</tr>
									</thead>
									<tbody id="linkList" style="color:black">
							
									</tbody>
								</table>
							</div>
						</div>
					</div>
										<!-- Modal Cadastro Link -->
                <div id="CadastrarLink" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel" style="color:#007486;">Cadastrar URL</h4>
                            </div>
                            <form action="{{ url('links/cadastrar') }}" method="POST">
                                {{ csrf_field() }}
								<input type="hidden" name="pkey" id="pkey" value="" />
                                <div class="modal-body">
									<div class="elementGroup">
                                        <div class="form-group row">
                                            <div class="col-md-4" style="color:#007486;"><label style="float: right;" >* Nome</label></div>
                                            <div class="col-md-6"><input type="text" class="form-control" name="nome" id="nome"  required></div>
                                        </div>
										<div class="form-group row">
                                            <div class="col-md-4" style="color:#007486;"><label style="float: right; " >* URL</label></div>
                                            <div class="col-md-6"><input type="text" class="form-control" name="endereco_url" id="endereco_url"  required></div>
											
                                        </div>
                                    </div>
									<hr>
								</div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                <!-- FIM Modal cadastro de link -->
				</div>
				<div id="d7" style="display: none; background: #003245; color: #fff;" class="elemento">
					<!-- Modal de Telefones Úteis -->
					<div id="ExibirTelefones" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<?php if(Auth::user()->permissao  == 1){?>
									<button id="NovoTelefone" class="btn btn-primary" style="float: right;" data-toggle="modal" onclick="javascript:CadastraFone();" >Novo Telefone</button>
									<?php } ?>
									<h4 class="modal-title" id="myModalLabel" style="color:black">Telefones Úteis</h4>
								</div>
					
								<table id="telefones" class="table table-striped table-bordered nowrap dataTablePTBR" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th style="color:black">NOME</th>
											<th style="color:black">TELEFONE</th>
											<th style="color:black">AÇÃO</th>
											</tr>
									</thead>
									<tbody id="telefoneList" style="color:black">
							
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- Modal Cadastro Telefone -->
                <div id="CadastrarTelefone" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel" style="color:#007486;">Cadastrar Telefone</h4>
                            </div>
                            <form action="{{ url('telefones/cadastrar') }}" method="POST">
                                {{ csrf_field() }}
								<input type="hidden" name="pkey" id="pkey" value="" />
                                <div class="modal-body">
									<div class="elementGroup">
                                        <div class="form-group row">
                                            <div class="col-md-4" style="color:#007486;"><label style="float: right;" >* Nome</label></div>
                                            <div class="col-md-6"><input type="text" class="form-control" name="nome" id="nome"  required></div>
                                        </div>
										<div class="form-group row">
                                            <div class="col-md-4" style="color:#007486;"><label style="float: right; " >* Telefone</label></div>
                                            <div class="col-md-6"><input type="text" class="form-control" name="telefone" id="telefone"  required></div>											
                                        </div>
                                    </div>
									<hr>
								</div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- FIM Modal cadastro de telefone -->
					
					
					
					
					<table id='tabela_telefone' class='table table-striped table-bordered nowrap dataTablePTBR hidetelefone ' cellspacing='0' width='100%'> 
						<thead> 	
							<tr> 
								<th>NOME</th>  
								<th>TELEFONE</th>
							</tr>
						</thead>
						<tbody id="telefone_uteis">

						</tbody>
					</table>
					
				</div>
			</div>
		</div>
	</div>
</div>
<input id="old" type="hidden" value="">
<!-- MENU HEADER - END -->

<!-- CONTENT -->
@yield('content')
<!-- CONTENT - END -->

<!-- FOOTER -->
<footer class="footer">
    <!--
	<div id="cont_b233357a1df48aaf159cbc3f4da03ab0"><script type="text/javascript" async src="https://www.tempo.com/wid_loader/b233357a1df48aaf159cbc3f4da03ab0"></script></div>
	<div class="container">
        
		<p class="text-muted">Melhor visualizado no Google Chrome, Firefox e Safari <img src="{{ url('Imobi/assets/img/trustlogo.png') }}"style="float: right; margin-top: -5px; border-radius: 15px;" /></p>
		<iframe scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="120" height="170" src="https://selos.climatempo.com.br/selos/MostraSelo120.php?CODCIDADE=558&SKIN=padrao"></iframe>
    </div>
	-->
</footer>
<!-- FOOTER - END -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ url('Imobi/assets/js/jquery-3.2.1.min.js') }}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ url('Imobi/assets/js/bootstrap.min.js') }}"></script>
<!-- ********************************** -->
<!-- Plugins -->
@yield('plugins')
<!-- ********************************** -->
<!-- Script -->
@yield('scripts')
<script>
	
	
	
</script>
<!-- ********************************** -->
</body>
</html>