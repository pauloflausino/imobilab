<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	Route::get('/', function () {
		return view('auth/login');
	});
	
	Auth::routes();
	Route::get('/logout', 'Auth\LoginController@logout');
	$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
	// Password reset link request routes...
	Route::get('forgot', ['as' =>'password/email', 'uses' => 'Auth\PasswordController@getEmail']);
	//Route::post('forgot', ['as' =>'password/email', 'uses' => 'Auth\PasswordController@postEmail']);
	Route::post('forgot', ['as' =>'password/email', 'uses' => 'Auth\PasswordController@postEmail']);
	// Password reset routes...
	Route::get('password/reset/{token}', 'Auth\SenhaController@getReset');
	Route::post('password/reset/nova', 'Auth\SenhaController@postReset');
	
	Route::group(['namespace' => 'Imobi'], function () {
		Route::group(['middleware' => 'auth'], function () {
		Route::get('/home', 'HomeController@index');

		Route::post('/imoveis/cadastrar', 'ImoveisController@cadastrar');
		Route::get('/imoveis/deletar/{id}', 'ImoveisController@deletar');
		Route::get('/imoveis/view/{id}', 'ImoveisController@view');
		Route::post('/imoveis/edit', 'ImoveisController@edit');
		Route::get('/imoveis/dash', 'ImoveisController@dash');
		});
	});