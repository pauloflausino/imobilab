function loadingPrefsMsgInterv() {
    return "<div class='paddingBox'><br><div class='tlfLoading' style='margin: 0 auto;'></div><br><p align='center'>Carregando informações</p><br></div>";
}

function addFieldIntervencoes(objThis, fieldName, fieldValue, pureField, eq, objForm, objl, fieldReply) {

    if (fieldValue == false || fieldValue == undefined) {
        fieldValue = "";
    }

    objBase = $(objThis).closest("tr");

    s = objBase.clone();

    wTextName = $(s).find("td:eq(0)").text().replace(":","");
    if (objl == undefined) {
        wTextName = wTextName + " " + Math.abs($(objForm).find("input[name='" + fieldName + "[]']").length+1);
    } else {
        wTextName = wTextName + " " + Math.abs(($(objForm).find("input[name='" + fieldName + "[]']").length+1));
    }


    $(s).find("td:eq(0)").text(wTextName);

    // remove add button
    $(s).find("a").remove();
    $(s).find("input").each(function(index, element){
        $(this).removeAttr("value");
    });

    // process values
    $(s).find("input[name='" + fieldName + "[]']").attr({"value":fieldValue});

    objFieldBase = $(objForm).find("input[name^='" + fieldName + "[]']:first-child").last();

    objAdded = objFieldBase.closest("tr").after("<tr>" + s.html() + "</tr>");

    refillIntervencoesField(objForm, fieldReply, fieldName);

}

function processIntervencoesField(objForm, fieldName, fieldReply) {

    objInput = objForm.find("input[name='" + fieldName + "']");

    objValue = objInput.val();
    objInput.attr({name: fieldName + "[]"}).after("&nbsp;<a href='javascript:void(0)' alt='" + fieldName + "'>Add</a>");
    objInput.parent().each(function(){
        $(this).find("a").each(function(){
            $(this).click(function(){
                objAlt = $(this).attr("alt");
                objThis = objForm.find("input[name^='" + objAlt + "']:eq(0)").parent();
                addFieldIntervencoes(objThis, fieldName, false, false, 0, objForm, 0, fieldReply);
            });
        });
    });

    if (objValue != undefined && objValue.indexOf(";") > 0) {

        objEachValue = objValue.split(";");

        objForm.find("input[name^='" + fieldName + "']:eq(0)").val(objEachValue[0]);
        total=objEachValue.length-1;
        for(i=1; i < objEachValue.length; i++) {
            addFieldIntervencoes(objForm.find("input[name^='" + fieldName + "']:eq(0)").parent(), fieldName, objEachValue[i], false, i, objForm, objEachValue.length, fieldReply);
        }

    }

}

function refillIntervencoesField(objForm, fieldName, refillFrom) {

    objInput = objForm.find("input[name='" + fieldName + "']");

    if (objInput.val() != undefined) {

        objValue = objInput.val();
        objInput.each(function(index,element){
            $(element).attr({name: fieldName + "[]"});
        });

        if (objValue.indexOf(";") > 0) {

            objEachValue = objValue.split(";");

            objForm.find("input[name^='" + fieldName + "']:eq(0)").val(objEachValue[0]);
            total=objEachValue.length-1;
            for(i=1; i < objEachValue.length; i++) {
                objForm.find("input[name^='" + fieldName + "']:eq(" + i + ")").val(objEachValue[i]);
            }

        }

    }

    if (refillFrom != undefined && refillFrom) {
        objForm.find("input[name^='" + refillFrom + "']").each(function(index, element){
            $(element).change(function(){
                objVal = $(this).val();
                objForm.find("input[name^='" + fieldName + "']:eq(" + index + ")").val(objVal);
            });
        });
    }

}

function intervSubmitEdit(objForm, idIntervencao, callbackf, bo) {

    objForm.submit(function(){

        //objForm.unbind("submit");
        objForm.find("input[type='submit']").val("AGUARDE...");

        $(objForm).find("#intervencaoErrosWindow").html("").hide();

        //tlfShowLockWindow();

        wUrlEditarIntervencao = baseUrlApp() + "/epmts/intervencoes/editar/send/" + idIntervencao;
        objSerialize = $(this).serialize();

        $.post(wUrlEditarIntervencao, objSerialize, function(callback){

            //intervSubmitEdit(objForm, idIntervencao, callbackf);
            objForm.find("input[type='submit']").val("Confirmar");
            //tlfHideLockWindow();

            pa = (callback.a != undefined) ? callback.a : false;
            pb = (callback.b != undefined) ? callback.b : false;

            if (pa && pa.status == "error") {
                $(objForm).find("#intervencaoErrosWindow").append(pa.msg).show();
            }

            if (pb && pb.status == "error") {
                $(objForm).find("#intervencaoErrosWindow").append(pb.msg).show();
            }

            msg = "";
            qntd = 0;
            sucess = false;

            if (pa && pa.status == "sucess") {
                msg += pa.detalhes;
                qntd = 1;
                sucess = true;
            }

            if (pb && pb.status == "sucess") {
                msg += pb.detalhes;
                qntd = 2;
                sucess = true;
            }

            if (msg != "" && sucess) {

                switch(qntd) {
                    case 1:
                        sMsg = "Você acabou de interagir com a intervenção abaixo:";
                    break;
                    case 2:
                        sMsg = "Você acabou de interagir com a intervenção abaixo:";
                    break;
                }

                alertMsg = "<div class='sm-plnb'><p align='center'></p><br><p class='cor-azul-claro' align='center'>" + sMsg + "</p></div>";
                alertMsg += msg;

                Boxy.alert(alertMsg, function(a){
                    if (callbackf != undefined) {
                        eval(callbackf);
                        bo.hide();
                    } else {
                        window.location.reload();
                    }
                });

            }


        }, "json");

    });

}

function interv_remove_pb(idIntervencaoA, idIntervencaoB) {
    Boxy.confirm("<h2>Notificação</h2><br><p align='center'>TEM CERTEZA QUE DESEJA REMOVER A 'PONTA B' DA INTERVENÇÃO?</p>", function(callback){
        wUrl = baseUrlApp() + "/epmts/intervencoes/remover/pontab/" + idIntervencaoA + "/" + idIntervencaoB;
        $.post(wUrl, {remover:1}, function(callback){

            alertMsg = "<div class='sm-plnb'><p class='cor-azul-claro' align='center'>" + callback.msg + "</p></div>";

            Boxy.alert("<h2>Notificação</h2><div class='sm-padding'>" + alertMsg + "</div>", function(a){
                if (callback.type == "sucess") {
                    window.location.hash = "detalhes," + idIntervencaoA;
                    window.location.reload();
                }
            });

        }, "json");
    })
}

function interv_edit(idIntervencao, callbackf) {

    bu = new Boxy(loadingPrefsMsgInterv(), {title: false, closeable: false, modal: true, unloadOnHide: true});

    wUrlIntervEditar = baseUrlApp() + "/epmts/intervencoes/editar/" + idIntervencao;

    $.get(wUrlIntervEditar, function(callback){

        bu.setContent(callback);
        bu.center();

        objContent = bu.getContent();

        objForm = objContent.find("form[name^='intervencoes_']");
        objForm.find("input[type='submit']").attr({"disabled":"disabled"});

        objForm.find("input[name='a[data_proforme]']").mask("99/99/9999");

        btnClose = objContent.find("a");

        btnClose.removeAttr("onclick").unbind("click").click(function(){
            Boxy.confirm("<h2>Notificação</h2><span class='tlf-cor-azul-padrao'>Tem certeza que deseja fechar a janela?</span>", function(){
                bu.hide();
            });
            return false;
        });

        intervCadOptionsByPrograma(objForm);

        intervSubmitEdit(objForm, idIntervencao, callbackf, bu);

    });

}

function intervSubmitCadastro(objForm, id) {

    objForm.submit(function(){

        objForm.unbind("submit");
        objForm.find("input[type='submit']").val("AGUARDE...");

        //tlfShowLockWindow();

        $(objForm).find("#intervencaoErrosWindow").html("").hide();

        objForm.find(".lock-window").trigger("click");

        if (id == undefined || !id) {
            wUrlCadastrarIntervencao = baseUrlApp() + "/epmts/intervencoes/cadastrar/send";
        } else {
            wUrlCadastrarIntervencao = baseUrlApp() + "/epmts/intervencoes/cadastrarb/send/" + id;
        }

        objSerialize = $(this).serialize();

        $.post(wUrlCadastrarIntervencao, objSerialize, function(callback){

            intervSubmitCadastro(objForm, id);
            objForm.find("input[type='submit']").val("Confirmar");
            //tlfHideLockWindow();

            pa = (callback.a != undefined) ? callback.a : false;
            pb = (callback.b != undefined) ? callback.b : false;
            if (pa && pa.status == "error" || pa && pa.status == undefined) {
                $(objForm).find("#intervencaoErrosWindow").append(pa.msg).show();
            }

            if (pb && pb.status == "error" || pb && pb.status == undefined) {
                $(objForm).find("#intervencaoErrosWindow").append(pb.msg).show();
            }

            if (pb && pb.status == "sucess" && callback.idIntervencaoPai != undefined && callback.idIntervencaoPai != 0) {
                window.location.hash = "detalhes," + callback.idIntervencaoPai;
            }

            msg = "";
            qntd = 0;

            if (pa && pa.status == "sucess") {
                msg += pa.detalhes;
                qntd = 1;
            }

            if (pb && pb.status == "sucess") {
                msg += pb.detalhes;
                qntd = 2;
            }

            if (msg != "") {

                switch(qntd) {
                    case 1:
                        sMsg = "Você acabou de interagir com a intervenção abaixo:";
                    break;
                    case 2:
                        sMsg = "Você acabou de interagir com a intervenção abaixo:";
                    break;
                }

                alertMsg = "<div class='sm-plnb'><p class='cor-azul-claro' align='center'> " + sMsg + "</p></div>";
                alertMsg += msg;

                Boxy.alert("<h2>Notificação</h2><div class='sm-padding'>" + alertMsg + "</div>", function(a){
                    window.location.reload();
                });

            }

        }, "json");

    });

}

function interv_cadastro(id) {

    wId = (id == undefined) ? false : id;

    bo = new Boxy(loadingPrefsMsgInterv(), {title: false, modal: true, unloadOnHide: true});

    if (wId) {
        wUrlIntervCadastro = baseUrlApp() + "/epmts/intervencoes/cadastrarb/" + wId;
    } else {
        wUrlIntervCadastro = baseUrlApp() + "/epmts/intervencoes/cadastrar";
    }

    $.get(wUrlIntervCadastro, function(callback){

        bo.setContent(callback);
        bo.center();

        objContent = bo.getContent();

        objForm = objContent.find("form[name^='intervencoes_']");
        objForm.find("input[name='a[data_proforme]']").mask("99/99/9999");

        btnClose = objContent.find("a");

        btnClose.removeAttr("onclick").unbind("click").click(function(){
            Boxy.confirm("<h2>Notificação</h2><span class='tlf-cor-azul-padrao'>Tem certeza que deseja fechar a janela?</span>", function(){
                bo.hide();
            });
            return false;
        });

        intervCadOptionsByPrograma(objForm);

        formatTipsy();

        tlfLoadLockWindow();

        intervSubmitCadastro(objForm, wId);

    });

}

function historicoOptionsInterv() {

    $(".interv-historico-detalhes-view").unbind("click").click(function(){

        id = $(this).attr("alt");

        bo = new Boxy(loadingPrefsMsgInterv(),{title:"",modal:true,unloadOnHide:true});

        wUrlDetalhesView = baseUrlApp() + "/epmts/intervencoes/historico/detalhes/" + id;
        $.get(wUrlDetalhesView, function(callback){

            bo.setContent(callback);
            bo.center();

        });

    });

}

function intervCadOptionsByPrograma(objForm) {

    processIntervencoesField(objForm, 'a[id_cpom]', 'b[id_cpom]');
    processIntervencoesField(objForm, 'a[pep_infraestrutura]', 1);
    processIntervencoesField(objForm, 'a[pep_equipamento]', 1);

    processIntervencoesField(objForm, 'a[espec_oem_num_oe]', 'b[espec_oem_num_oe]');

    processIntervencoesField(objForm, 'a[de_para]', 'b[de_para]');

    processIntervencoesField(objForm, 'a[numero_sci]', 1);
    /*
    refillIntervencoesField(objForm, 'b[id_cpom]', 'a[id_cpom]');
    refillIntervencoesField(objForm, 'b[pep_infraestrutura]', 1);
    refillIntervencoesField(objForm, 'b[pep_equipamento]', 1);

    refillIntervencoesField(objForm, 'b[espec_oem_num_oe]', 'a[espec_oem_num_oe]');
    refillIntervencoesField(objForm, 'b[numero_sci]', 1);
    */
    $(".interv-historico").unbind("click").click(function(){

        objAlt = $(this).attr("alt");

        objClick = $(this);

        wUrlHistoricoView = baseUrlApp() + "/epmts/intervencoes/historico/" + objAlt;

        b = new Boxy(loadingPrefsMsgInterv(), {title:"", modal: true, unloadOnHide: true});

        $.get(wUrlHistoricoView, function(callback){
            b.setContent(callback);
            formatGrids();
            b.center();
            historicoOptionsInterv();
        });

    });

    idIntervencaoA = ($(objForm).find(".id_intervencao_a").val() == undefined) ? "" : $(objForm).find(".id_intervencao_a").val();
    idIntervencaoB = ($(objForm).find(".id_intervencao_b").val() == undefined) ? "" : $(objForm).find(".id_intervencao_b").val();

    // programa
    objFieldPrograma = $(objForm).find("select[name='a[programa]']");
    objFieldPrograma.change(function(){

        objValue = $(this).val();
        
        objNamePrograma = $(this).attr("name");

        objNamePrograma1 = objNamePrograma.replace("a[","");
        objNamePrograma2 = objNamePrograma1.replace("]","");

        $.ajaxSetup({async:true});

        // get programa details
        wUrlProgramaDetails = baseUrlApp() + "/epmts/fp/programas/detailsJson/" + objValue;

        $.getJSON(wUrlProgramaDetails, function(callback){

            $(objForm).find(".cad_ponta_a").find("input[name='a[tipo_programa]']").val(callback.programa_name);

            $(objForm).find(".idTipoPrograma").val(callback.tipo_programa_cod);

            if (callback.tipo_programa_cod != undefined && callback.tipo_programa_cod == 1) {

                $(objForm).find(".id_enlace_option").show();
                $(objForm).find(".frequencia_rf_option").hide();
                $(objForm).find(".frequencia_tm_option").show();
                $(objForm).find(".diametro_option").show();
                $(objForm).find(".azimute_option").show();

            } else {

                $(objForm).find(".id_enlace_option").hide();
                $(objForm).find(".frequencia_rf_option").show();
                $(objForm).find(".frequencia_tm_option").hide();
                $(objForm).find(".diametro_option").hide();
                $(objForm).find(".azimute_option").hide();

            }
            //console.log(callback);
            /** -- REGIONAIS -- */
            wUrlRegionais = baseUrlApp() + "/epmts/fp/tecnologias/getAllByPrograma/" + callback.tipo_programa_cod + "/" + idIntervencaoA;
            objForm.find("select[name='a[tecnologia]']").html("<option value=''>CARREGANDO...</option>");
            $.get(wUrlRegionais, function(callback){
                objForm.find("select[name='a[tecnologia]']").html(callback);
            });

            /** -- FORNECEDORES -- */

            /** -- Infraestrutura -- */
            objValInfraEdit = $(objForm).find("#player-infraestrutura-selecionado").val();
            wUrlFornecedoresTransmissao = baseUrlApp() + "/epmts/fp/players/getAllByPrograma/0/" + callback.tipo_programa_cod + "/" + objValInfraEdit;
            objForm.find("select[name='a[player_infraestrutura]']").html("<option value=''>CARREGANDO...</option>");
            $.get(wUrlFornecedoresTransmissao, function(callback){
                objForm.find("select[name='a[player_infraestrutura]']").html(callback);
            });

            /** -- Equipamento -- */
            objValEquipEdit = $(objForm).find("#player-equipamento-selecionado").val();
            wUrlFornecedoresEquipamento = baseUrlApp() + "/epmts/fp/players/getAllByPrograma/1/" + callback.tipo_programa_cod + "/" + objValEquipEdit;
            objForm.find("select[name='a[player_equipamento]']").html("<option value=''>CARREGANDO...</option>");
            $.get(wUrlFornecedoresEquipamento, function(callback){
                objForm.find("select[name='a[player_equipamento]']").html(callback);
            });

        });

    });

    objFieldUFa = $(objForm).find("select[name='a[unid_fed]']");
    objFieldUFa.change(function(){

        objValue = $(this).val();

        if (objValue.match(/\S/) && objValue != "null") {

            wUrlRegionais = baseUrlApp() + "/epmts/fp/regionais/getAllByUf/" + objValue; // + "/" + idIntervencaoA;
            objForm.find("select[name='a[regional]']").html("<option value=''>CARREGANDO...</option>");
            $.get(wUrlRegionais, function(callback){

                objForm.find("select[name='a[regional]']").html(callback);

                objValRegional = objForm.find(".regional-id").val();

                if (objValRegional.match(/\S/)) {
                    objForm.find("select[name='a[regional]']").val(objValRegional);
                }

                $(objForm).find("select[name='a[regional]']").trigger("change");

            });

        }

    });
    objFieldUFa.trigger("change");

    objFieldUFb = $(objForm).find("select[name='b[unid_fed]']");
    objFieldUFb.change(function(){

        objValue = $(this).val();
        if (objValue.match(/\S/) && objValue != "null") {
            wUrlRegionais = baseUrlApp() + "/epmts/fp/regionais/getAllByUf/" + objValue; // + "/" + idIntervencaoB;
            objForm.find("select[name='b[regional]']").html("<option value=''>CARREGANDO...</option>");
            $.get(wUrlRegionais, function(callback){
                objForm.find("select[name='b[regional]']").html(callback);
                $(objForm).find("select[name='b[regional]']").trigger("change");
            });
        }

    });
    objFieldUFb.trigger("change");

    objRegionalA = objForm.find("select[name='a[regional]']");
    objRegionalA.change(function(){

        objValue = $(this).val();

        if (objValue.match(/\S/) && objValue != "null") {

            ufSelected = $(objForm).find("select[name='a[unid_fed]'] option:selected").val();

            $("#ac_vivo-a").unbind();
            $("#ac_vivo-a").attr({"disabled":"disabled"});

            $("#label-id-vivo-a").text("CARREGANDO...");

            wUrlRegionais = baseUrlApp() + "/epmts/sites/getAllByRegional/" + objValue + "/" + ufSelected;

            $.getJSON(wUrlRegionais,function(data2) {

                $("#ac_vivo-a").autocomplete(data2, {
                    width: 300,
                    minChars:0,
                    selectFirst: true,
                    multiple: false,
                    formatItem: function(data, i, n, value) {
                        return data.id_vivo;
                    },
                    formatResult: function(data) {
                        return data.id_vivo;
                    }
                }).result(function(event, data) {
                    objForm.find("input[name='a[id_vivo]']").val(data.sites_id).change();
                });

                $("#ac_vivo-a").removeAttr("disabled");
                $("#label-id-vivo-a").text("");

            });

        }

    });

    objRegionalB = objForm.find("select[name='b[regional]']");
    objRegionalB.unbind("change").change(function(){

        objValue = $(this).val();

        if (objValue != null && objValue.match(/\S/)) {

            /*
            wUrlRegionais = baseUrlApp() + "/epmts/sites/getAllByRegional/" + objValue + "/" + idIntervencaoB;
            objForm.find("select[name='b[id_vivo]']").html("<option value=''>CARREGANDO...</option>");

            //tlfShowLockWindow();

            $.get(wUrlRegionais, function(callback){
                objForm.find("select[name='b[id_vivo]']").html(callback);
                //objForm.find("select[name='b[id_vivo]']").trigger("change");
                //tlfHideLockWindow();
            });*/

            //objForm.find("input[name='b[id_vivo]']").val("");
            objForm.find("input#ac_vivo-b").val("");

            $(objForm).find("input[name='b[identificacao]']").val("");
            $(objForm).find("input[name='b[id_enlace]']").val("");

            ufSelected = $(objForm).find("select[name='b[unid_fed]'] option:selected").val();

            $("#ac_vivo-b").unbind();
            $("#ac_vivo-b").attr({"disabled":"disabled"});

            $("#label-id-vivo-b").text("CARREGANDO...");

            wUrlRegionais = baseUrlApp() + "/epmts/sites/getAllByRegional/" + objValue + "/" + ufSelected;

            $.getJSON(wUrlRegionais,function(data2) {

                $("#ac_vivo-b").autocomplete(data2, {
                    width: 300,
                    minChars:0,
                    selectFirst: true,
                    multiple: false,
                    formatItem: function(data, i, n, value) {
                        return data.id_vivo;
                    },
                    formatResult: function(data) {
                        return data.id_vivo;
                    }
                }).result(function(event, data) {
                    objForm.find("input[name='b[id_vivo]']").val(data.sites_id).change();
                });

                $("#ac_vivo-b").removeAttr("disabled");
                $("#label-id-vivo-b").text("");

            });

        }

    });


    objFieldIdVivoA = $(objForm).find("input[name='a[id_vivo]']");
    objFieldIdVivoA.change(function(){

        //console.log("oie");

        //objValu = $(objForm).find("a[name='b[id_vivo]'] option:selected").text();

        objVal = $(this).val();

        objValue = $(objForm).find("input#ac_vivo-a").val();
        objValueA = $(objForm).find("select[name='a[unid_fed]'] option:selected").text();

        if (objValue.match(/\S/)) {

            $(objForm).find("input[name='a[identificacao]']").val(objValueA + objValue);

            wUrlInfoSite = baseUrlApp() + "/epmts/sites/getInfoDetails/" + objVal
            $.getJSON(wUrlInfoSite, function(callbackJson){
                if (callbackJson.siteObtido) {

                    valModalidade = callbackJson.siteObtido.modalidade;

                    if (valModalidade) {

                        /** -- Fornecedores aquisição -- */
                        objValEdit = $(objForm).find("#player-aquisicao-selecionado").val();
                        wUrlFornecedoresAquisicao = baseUrlApp() + "/epmts/fp/players/getAllByModalidade/" + valModalidade + "/" + objValEdit;
                        objForm.find("select[name='a[player_aquisicao]']").html("<option value=''>CARREGANDO...</option>");
                        $.get(wUrlFornecedoresAquisicao, function(callback){
                            objForm.find("select[name='a[player_aquisicao]']").html(callback);
                        });

                    }

                }
            });

        } else {
            $(objForm).find("input[name='a[identificacao]']").val("");
        }

        /*
        if (
            objValue != "-- SELECIONE --" &&
            objValueA != "-- SELECIONE --" &&
            objValue != "CARREGANDO..."
        ) {


            if (
                objValu != "-- SELECIONE --" &&
                objValue != "CARREGANDO..."
            ) {
                //$(objForm).find("input[name='a[id_enlace]']").val(objValu + "-" + objValue);
                //$(objForm).find("input[name='b[id_enlace]']").val(objValu + "-" + objValue);
            }
        }
        */
        //tipoPrograma = $(objForm).find(".idTipoPrograma").val();
        /*
        if ($.active == 1) {

        }*/

        objForm.find("input[type='submit']").removeAttr("disabled");

    }).trigger("change");

    objFieldIdVivoB = $(objForm).find("input[name='b[id_vivo]']");

    objFieldIdVivoB.change(function(){

        objValu = $(objForm).find("span.id-vivo-a").text();
        objValue = $(objForm).find("input#ac_vivo-b").val();

        objValueB = $(objForm).find("select[name='b[unid_fed]'] option:selected").text();

        if (
            objValue != "-- SELECIONE --" &&
            objValueB != "-- SELECIONE --"
        ) {
            $(objForm).find("input[name='b[identificacao]']").val(objValueB + objValue);
            if (objValu != "-- SELECIONE --") {
                //$(objForm).find("input[name='a[id_enlace]']").val(objValu + "-" + objValue);
                $(objForm).find("input[name='b[id_enlace]']").val(objValu + "-" + objValue);
            }
        }

        tipoPrograma = $(objForm).find(".idTipoPrograma").val();

        objForm.find("input[type='submit']").removeAttr("disabled");

    });

    objFieldSciA = $(objForm).find("select[name='a[tipo_sci]']");
    objFieldSciA.change(function(){

        objValue = $(this).val();

        objText = $("select[name='a[tipo_sci]'] option:selected").text();

        if (objValue.match(/\S/) && objText == "SCI") {
            $(".field-numero-sci").show();
        } else {
            $(".field-numero-sci").hide();
        }

    });
    objFieldSciA.trigger("change");

    objFieldSciB = $(objForm).find("select[name='b[tipo_sci]']");
    objFieldSciB.change(function(){

        objValue = $(this).val();

        objText = $("select[name='b[tipo_sci]'] option:selected").text();

        if (objValue.match(/\S/) && objText == "SCI") {
            $(".field-numero-sci").show();
        } else {
            $(".field-numero-sci").hide();
        }

    });
    objFieldSciB.trigger("change");


    /*
    // regional
    objFieldRegionalA = $(objForm).find("select[name='a[regional]']");
    objFieldRegionalA.change(function(){

        objValue = $(this).val();

        if (objValue.match(/\S/)) {
            wUrlRegionalDetails = baseUrlApp() + "/epmts/fp/regionais/detailsJson/" + objValue;
            $.getJSON(wUrlRegionalDetails, function(callback){

                if (callback) {
                    objForm.find("input[id='a[unid_fed]']").val(callback.unid_fed);
                    objForm.find("input[name='a[unid_fed]']").val(callback.id_unid_fed);
                }
            });
        }

    });

    // regional b
    objFieldRegionalB = $(objForm).find("select[name='b[regional]']");
    objFieldRegionalB.change(function(){

        objValue = $(this).val();

        if (objValue.match(/\S/)) {
            wUrlRegionalDetails = baseUrlApp() + "/epmts/fp/regionais/detailsJson/" + objValue;
            $.getJSON(wUrlRegionalDetails, function(callback){
                if (callback) {
                    objForm.find("input[id='b[unid_fed]']").val(callback.unid_fed);
                    objForm.find("input[name='b[unid_fed]']").val(callback.id_unid_fed);
                }
            });
        }

    });
    */

    objFieldPrograma.trigger("change");

}
