function addFieldSites(objThis, fieldName, fieldValue, pureField) {

    if (fieldValue == false || fieldValue == undefined) {
        fieldValue = "";
    }

    if (pureField == undefined || pureField == false) {
        objThis.after("<div class='f-field-text field-required' style='display: block;'><div class='labelField tlf-cor-azul-padrao' style='width: 200px;'>-</div><input type='text' name='" + fieldName + "[]' value='" + fieldValue + "' id='sm-" + fieldName + "'></div>");
    } else {
        objThis.after("<input type='text' name='" + fieldName + "[]' value='" + fieldValue + "' id='sm-" + fieldName + "'>");
    }

}

function processSitesField(objFormSites, fieldName, pureField) {
    objInput = objFormSites.find("input[name='" + fieldName + "']");
    objValue = objInput.val();
    objInput.attr({name: fieldName + "[]"}).after("&nbsp;<a href='javascript:void(0)' class='add-option-" + fieldName + "' alt='" + fieldName + "'>Add</a>");
    objInput.parent().each(function(){
        $(this).find("a.add-option-" + fieldName).each(function(){
            $(this).click(function(){
                objAlt = $(this).attr("alt");
                objThis = objFormSites.find("input[name^='" + objAlt + "']:eq(0)").parent();
                addFieldSites(objThis, fieldName, false, pureField);
            });
        });
    });

    if (objValue.indexOf(";") > 0) {

        objEachValue = objValue.split(";");

        objFormSites.find("input[name^='" + fieldName + "']:eq(0)").val(objEachValue[0]);

        for(i=1; objEachValue.length > i; i++) {
            addFieldSites(objFormSites.find("input[name^='" + fieldName + "']:eq(0)").parent(), fieldName, objEachValue[i], pureField);
        }

    }

}

function formSites(cg) {

	objFormSites = $("form[name^='sites_']");

	objFormSites.find("select[name='unid_fed']").parent().prepend("<br><div style='width:200px;text-align:right;' class='sm-padding tlf-cor-azul-padrao'><b>IDENTIFICAÇÃO</b></div><br>");
	objFormSites.find("select[name='municipio']").parent().prepend("<br><div style='width:200px;text-align:right;' class='sm-padding tlf-cor-azul-padrao'><b>LOCALIZAÇÃO</b></div><br>");
	objFormSites.find("select[name='modalidade']").parent().prepend("<br><div style='width:200px;text-align:right;' class='sm-padding tlf-cor-azul-padrao'><b>ESTRUTURA</b></div><br>");
	objFormSites.find("input[name='numero_urbanistico']").parent().prepend("<br><div style='width:200px;text-align:right;' class='sm-padding tlf-cor-azul-padrao'><b>LICENCIAMENTO</b></div><br>");
	objFormSites.find("input[name='concessionaria']").parent().prepend("<br><div style='width:200px;text-align:right;' class='sm-padding tlf-cor-azul-padrao'><b>ENERGIA</b></div><br>");

	objFormSites.find("input[name='latitude']").parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("input[name='longitude']").parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("select[name='modalidade']").parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("select[name='detentora']").parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("select[name='tipo_site']").parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("select[name='tipo_infra']").parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("input[name='numero_urbanistico']").attr({"placeholder" : "INDEFINIDO"}).parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("input[name='numero_ambiental']").attr({"placeholder" : "INDEFINIDO"}).parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("input[name='numero_comar']").attr({"placeholder" : "INDEFINIDO"}).parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("input[name='numero_comar']").attr({"placeholder" : "INDEFINIDO"}).parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("input[name='numero_medidor']").attr({"placeholder" : "INDEFINIDO"}).parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("input[name='numero_instalacao']").attr({"placeholder" : "INDEFINIDO"}).parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("input[name='numero_contrato_sap']").attr({"placeholder" : "INDEFINIDO"}).parent().show().removeClass("field-nonrequired").addClass("field-required");

	objFormSites.find("input[name='concessionaria']").attr({"placeholder" : "INDEFINIDO"}).parent().show().removeClass("field-nonrequired").addClass("field-required");

	objFormSites.find("input[name='nome_site']").parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("input[name='endereco']").parent().show().removeClass("field-nonrequired").addClass("field-required");

	objFormSites.find("input[name='id_master']").parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("input[name='id_sequencial']").parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("input[name='id_detentora']").parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("input[name='numero_ssi']").parent().show().removeClass("field-nonrequired").addClass("field-required");
	objFormSites.find("select[name='distrito']").parent().show().removeClass("field-nonrequired").addClass("field-required");

	objFormSites.find("input[name='id_vivo']").change(function(){
		valIdVivo = $(this).val();
		ufVal = objFormSites.find("select[name='unid_fed'] option:selected").text();
		if (ufVal != "-- SELECIONE --") {
			objFormSites.find("input[name='identificacao']").val(ufVal.trim() + valIdVivo.trim());
		}
	});

	objFormSites.find("select[name='unid_fed']").change(function(){
		ufVal = objFormSites.find("select[name='unid_fed'] option:selected").text();
		valIdVivo = objFormSites.find("input[name='id_vivo']").val();
		objFormSites.find("input[name='identificacao']").val(ufVal.trim() + valIdVivo.trim());
	});

	objFormSites.find(".labelField").css({"width":"200px"});

	// colors
	objFormSites.find("input[name='identificacao']").addClass("sm-color-bg-gray");

	// fields duplicate
	processSitesField(objFormSites, 'numero_urbanistico');
	processSitesField(objFormSites, 'numero_ambiental');
	processSitesField(objFormSites, 'numero_comar');

	btnClose = cg.window.getContent().find("div:eq(0)").find("a:eq(0)");

	formatTipsy();

    btnClose.removeAttr("onclick").unbind("click").click(function(){
    	Boxy.confirm("<h2>Notificação</h2><span class='tlf-cor-azul-padrao'>Tem certeza que deseja fechar a janela?</span>", function(){
            cg.window.hide();
        },{title:""});
        return false;
    });

	cg.window.center();

}
