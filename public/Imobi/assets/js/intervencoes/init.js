function _sa_baseUrl() {
    return $("#sa-baseurl").val();
}

/**
 * Fixes
 */

jQuery.curCSS = jQuery.css;

/*
 * Function to get a base url.
 * return string;
 */
function baseUrlApp() {
    return $("#sa-baseurl").val() + "/napp";
}

function appOpened() {
    return $("#sa-appname").val();
}

function alertClose() {
    $("#blocoMessageAlert").fadeOut("slow");
}

function _validate_json(d) {
    if (d != undefined) {
        return !(/[^,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]/.test(d.replace(/"(\\.|[^"\\])*"/g,'')));
    } else {
        return false;
    }

}

function _sa_showMessage(header,msg) {
    $.jGrowl(msg,{
        header: header
    });
}

function _sa_formatTipsy() {
    $(".tipsy-s").tipsy({gravity:'s', html: true});
    $(".tipsy-w").tipsy({gravity:'w', html: true});
    $(".tipsy-l").tipsy({gravity:'l', html: true});
    $(".tipsy-n").tipsy({gravity:'n', html: true});
}


function _sa_formatButtons() {
    $(".button").button();
    $(".button-cancel").button({
        icons: {
            primary: "ui-icon-cancel"
        }
    });
    $(".button-confirm").button({
        icons: {
            secondary: "ui-icon-plus"
        }
    });
    $(".button-search").button({
        icons: {
            primary: "ui-icon-search"
        }
    });
    _sa_formatTipsy();
}

function _sa_desactiveButtonSubmitAfterAction(obj) {
    obj.find("form").each(function(){
        objForm = $(this);
        objForm.find("input[name='buildRequest']").parent().find("input[type='submit']").each(function(){
            $(this).click(function(){
                objForm.find("input[name='buildRequest']").parent().find("input[type='submit']").attr({"disabled":"disabled"}).val("Aguarde ...");
                objForm.trigger("submit");
                return true;
            });
        });
    });
}

function _sa_setDesignButtons() {
    $(".minibutton, .minibutton>span, .activity-loading, .activity-loading>span").css({"background-image":"url('" + $("input[name='minibuttons-matrix']").val() + "')"});
    $("a[class*='btn-']").find(".icon").css({"background-image":"url('" + $("input[name='minibuttons-icons']").val() + "')"});
}

function _sa_ContentFormRequiredFields(f, b) {
    if (b == undefined) { b = false; }

    f.find(".field-nonrequired").hide();
    f.find(".showOptionalFields").each(function(){

        $(this).click(function(){

            objClick = $(this);

            if (objClick.is(":checked")) {
               f.find(".field-nonrequired").show();
               f.find(".showOptionalFields").attr({"checked":"checked"});
            } else {
               f.find(".field-nonrequired").hide();
               f.find(".showOptionalFields").removeAttr("checked");
            }

            if (b) {
                b.center();
            }

       });

    });
}

function _sa_loadComponents(obj) {

    $(obj).sm_mask();

    $(obj).find(".smartComponentGrid").each(function(){
       tableTitle = $(this).attr("title");
       $(this).flexigrid({height:'175', width:'auto',striped:true, singleSelect: false, resizable: false});
    });

}


function _sa_AutoCompleteLoadDetails(objForm, objApp, objField, objValue) {
    data = baseUrlApp() + "/" + objApp + "/_ac/details";
    $.post(data, {form: objForm, field: objField, v: objValue}, function(c){
        b = new Boxy(c, {title: "Mais detalhes", modal: true});
        b.center();
    });
}
/*
function _sa_AutoCompleteSelect(objForm) {

    getAllFields = $("input[name^='interactField-']");
    getAllFields.each(function(){

        objFieldName = $(this).attr("name").replace("interactField-","");
        objFieldValue = $(this).val();

        //$("select[name='" + objFieldName + "']").attr({"disabled":"disabled"});
        //console.log($("select[name='" + objFieldName + "']"));

        fieldAttrAutoCompleteInteractField = ($("input[name^='interactField-" + objFieldName + "']").val() == undefined) ? false : $("input[name^='interactField-" + objFieldName + "']").val();

        $("input[name='" + objFieldValue + "']").bind("change", function(){

            $("select[name='" + objFieldName + "']").html("<option>Carregando ...</option>");

            fieldAttrAutoCompleteInteractField = ($("input[name^='interactField-" + objFieldName + "']").val() == undefined) ? false : $("input[name^='interactField-" + objFieldName + "']").val();

            objInteractField = objFieldValue;
            objInteractKey = $("input[name='interactKey-" + objFieldName + "']").val();

            referenceApp = $("input[name='_ref_" + objFieldName + "-app']").val();
            referenceForm = $("input[name='_ref_" + objFieldName + "-form']").val();
            referenceField = $("input[name='_ref_" + objFieldName + "-field']").val();
            referenceIndex = $("input[name='_ref_" + objFieldName + "-index']").val();

            objParamsPost = {
                field : objInteractField,
                key : objInteractKey,
                ref: function() {
                    return $("input[name='" + fieldAttrAutoCompleteInteractField + "']").val();
                },
                rform : referenceForm,
                rfield : referenceField,
                rindex : referenceIndex,
                rapp : referenceApp
            };
            //console.log(objParamsPost);

            // get all records
            data = baseUrlApp() + "/" + appOpened() + "/_ac/select/options";
            $.post(
                data,
                objParamsPost,
                function(c){

                    //console.log(fieldAttrAutoCompleteInteractField);
                    // process records and create options to put on select
                    strOptionsSelect = "<option value='0'> -- selecione -- </option>";
                    $(c).each(function(index, element){

                        strSelected = false;
                        objStrFieldValue = $("input[name='interactFieldValue-" + objFieldName + "']").val();
                        if (element.v == objStrFieldValue) {
                            strSelected = "selected='selected'";
                        }

                        strOptionsSelect += "<option value=" + element.v + " " + strSelected + "> " + element.l + " </option>";
                    });

                    $("select[name='" + objFieldName + "']").html(strOptionsSelect);
                    //$("select[name='" + objFieldName + "']").removeAttr("disabled");

                },
                "json"
            );

        });

        valueFieldInteract = $("input[name='" + fieldAttrAutoCompleteInteractField + "']").val();
        if (valueFieldInteract.match(/\S/)) {
            $("input[name='" + fieldAttrAutoCompleteInteractField + "']").trigger("change");
        }

    });

}

function _sa_AutoCompleteSelect(objForm) {

    getAllFields = $("input[name^='interactField-']");
    getAllFields.each(function(){

        objFieldName = $(this).attr("name").replace("interactField-","");
        objFieldValue = $(this).val();

        fieldAttrAutoCompleteInteractField = (objForm.find("input[name^='interactField-" + objFieldName + "']").val() == undefined) ? false : objForm.find("input[name^='interactField-" + objFieldName + "']").val();

        objFormInteractField = ($("input[name='" + fieldAttrAutoCompleteInteractField + "']").val() == undefined) ? $("select[name='" + fieldAttrAutoCompleteInteractField + "']") : $("input[name='" + fieldAttrAutoCompleteInteractField + "']");

        objFormInteractField.bind("change", function(){

            objForm.find("select[name='" + objFieldName + "']").html("<option>Carregando ...</option>");

            fieldAttrAutoCompleteInteractField = (objForm.find("input[name^='interactField-" + objFieldName + "']").val() == undefined) ? false : objForm.find("input[name^='interactField-" + objFieldName + "']").val();

            objInteractField = objFieldValue;
            objInteractKey = objForm.find("input[name='interactKey-" + objFieldName + "']").val();

            referenceApp = objForm.find("input[name='_ref_" + objFieldName + "-app']").val();
            referenceForm = objForm.find("input[name='_ref_" + objFieldName + "-form']").val();
            referenceField = objForm.find("input[name='_ref_" + objFieldName + "-field']").val();
            referenceIndex = objForm.find("input[name='_ref_" + objFieldName + "-index']").val();

            objParamsPost = {
                field : objInteractField,
                key : objInteractKey,
                ref: function() {

                    objInteractField = ($("input[name='" + fieldAttrAutoCompleteInteractField + "']").val() == undefined) ? $("select[name='" + fieldAttrAutoCompleteInteractField + "']").val() : $("input[name='" + fieldAttrAutoCompleteInteractField + "']").val();

                    return objInteractField;
                },
                rform : referenceForm,
                rfield : referenceField,
                rindex : referenceIndex,
                rapp : referenceApp
            };

            // get all records
            data = baseUrlApp() + "/" + referenceApp + "/_ac/select/options";
            $.post(
                data,
                objParamsPost,
                function(c){

                    // process records and create options to put on select
                    strOptionsSelect = "<option value=''> -- selecione -- </option>";
                    $(c).each(function(index, element){

                        strSelected = false;
                        objStrFieldValue = objForm.find("input[name='interactFieldValue-" + objFieldName + "']").val();
                        if (element.v == objStrFieldValue) {
                            strSelected = "selected='selected'";
                        }

                        strOptionsSelect += "<option value=" + element.v + " " + strSelected + "> " + element.l + " </option>";
                    });

                    objForm.find("select[name='" + objFieldName + "']").html(strOptionsSelect);

                },
                "json"
            );

        });

        valueFieldInteract = (objForm.find("input[name='" + fieldAttrAutoCompleteInteractField + "']").val() == undefined) ? objForm.find("select[name='" + fieldAttrAutoCompleteInteractField + "']") : objForm.find("input[name='" + fieldAttrAutoCompleteInteractField + "']");

        if (valueFieldInteract.val().match(/\S/)) {
            valueFieldInteract.trigger("change");
        }

    });

}*/

function _sa_AutoCompleteSelect(objForm) {

    arrayFields = Array();
    i=0;
    getAllFields = $(objForm).find("input[name^='interactField-']");

    getAllFields.each(function(){

        objFieldName = $(this).attr("name").replace("interactField-","");
        objFieldValue = $(this).val();
        fieldAttrAutoCompleteInteractField = (objForm.find("input[name^='interactField-" + objFieldName + "']").val() == undefined) ? false : objForm.find("input[name^='interactField-" + objFieldName + "']").val();

        objFormInteractField = (objForm.find("input[name='" + fieldAttrAutoCompleteInteractField + "']").val() == undefined) ? objForm.find("select[name='" + fieldAttrAutoCompleteInteractField + "']") : objForm.find("input[name='" + fieldAttrAutoCompleteInteractField + "']");

        arrayFields[i] = objFieldName;
        i++;

        objFormInteractField.unbind("change");
        objFormInteractField.bind("change", function(){

            bindedField = $(this).attr("name");

            for(x=0; x < arrayFields.length; x++) {

                objFieldName = arrayFields[x];

                fieldAttrAutoCompleteInteractField = (objForm.find("input[name^='interactField-" + objFieldName + "']").val() == undefined) ? false : objForm.find("input[name^='interactField-" + objFieldName + "']").val();

                if (bindedField == fieldAttrAutoCompleteInteractField) {

                    objInteractField = objFieldValue;
                    objInteractKey = objForm.find("input[name='interactKey-" + objFieldName + "']").val();

                    //console.log(objInteractKey);

                    referenceApp = objForm.find("input[name='_ref_" + objFieldName + "-app']").val();
                    referenceForm = objForm.find("input[name='_ref_" + objFieldName + "-form']").val();
                    referenceField = objForm.find("input[name='_ref_" + objFieldName + "-field']").val();
                    referenceIndex = objForm.find("input[name='_ref_" + objFieldName + "-index']").val();
                    referenceBind = objForm.find("input[name='_ref_" + objFieldName + "-bind']").val();

                    objParamsPost = {
                        field : objInteractField,
                        key : objInteractKey,
                        ref: function() {
                            objInteractField = (objForm.find("input[name='" + fieldAttrAutoCompleteInteractField + "']").val() == undefined) ? objForm.find("select[name='" + fieldAttrAutoCompleteInteractField + "']").val() : objForm.find("input[name='" + fieldAttrAutoCompleteInteractField + "']").val();
                            /*
                            objValueInteract = objForm.find("input[name^='interactFieldValue-" + fieldAttrAutoCompleteInteractField + "']").val();
                            if (objValueInteract != undefined) {
                                if (objValueInteract.match(/\S/) != null) {
                                    objInteractField = objValueInteract;
                                }
                            }*/
                            return objInteractField;
                        },
                        rform : referenceForm,
                        rfield : referenceField,
                        rindex : referenceIndex,
                        rapp : referenceApp,
                        rbind : referenceBind,
                        sdt : objForm.serialize()
                    };

                    // get all records
                    data = baseUrlApp() + "/" + referenceApp + "/_ac/select/options";

                    objForm.find("select[name='" + objFieldName + "']").html("<option>Carregando ...</option>");

                    r = $.post(
                        data,
                        objParamsPost,
                        function(c){

                            vBind = false;
                            //console.log(c[0].b);
                            // process records and create options to put on select
                            if(c[0].b == 'regional' || c[0].b == 'cod_ibge'){    
                                strOptionsSelect = "";
                            }else{    
                                strOptionsSelect = "<option value=''> -- selecione -- </option>";
                            }

                            $(c).each(function(index, element){
                                if (element.l != undefined) {
                                    vBind = element.b;
                                    strSelected = false;
                                    objStrFieldValue = objForm.find("input[name='interactFieldValue-" + element.b + "']").val();

                                    if (element.v == objStrFieldValue) {
                                        strSelected = "selected='selected'";
                                    }
                                    strOptionsSelect += "<option value='" + element.v + "' " + strSelected + "> " + element.l + " </option>";
                                } else {
                                    vBind = element.b;
                                }
                            });

                            if (vBind) {
                                objForm.find("select[name='" + vBind + "']").html(strOptionsSelect);
                            }

                        },
                        "json"
                    );

                }

            }

        });

        if (fieldAttrAutoCompleteInteractField && fieldAttrAutoCompleteInteractField.match(/\S/)) {
            valueFieldInteract = (objForm.find("input[name='" + fieldAttrAutoCompleteInteractField + "']").val() == undefined) ? objForm.find("select[name='" + fieldAttrAutoCompleteInteractField + "']") : objForm.find("input[name='" + fieldAttrAutoCompleteInteractField + "']");

            if (valueFieldInteract.val().match(/\S/)) {
                valueFieldInteract.trigger("change");
            }
        }

    });

}

function _sa_AutoCompleteLoad(objForm) {

    _sa_AutoCompleteSelect(objForm);

    $("input[class^='_sm-ac-field']").each(function(){

        fieldName = $(this).attr("name");
        objField = $(this);

        fieldShowAutoComplete = $("input[name='" + fieldName + "']");

        fieldAttrAutoCompleteForm = $("input[name^='" + fieldName + "-form']").val();
        fieldAttrAutoCompleteApp = $("input[name^='" + fieldName + "-app']").val();
        fieldAttrAutoCompleteField = $("input[name^='" + fieldName + "-field']").val();
        fieldAttrAutoCompleteFormatData = objForm.find("input[name^='" + fieldName + "-formatData']").val();
        fieldAttrAutoCompleteIndex = $("input[name^='" + fieldName + "-indice']").val();
        fieldAttrAutoCompleteSearchType = $("input[name^='" + fieldName + "-searchType']").val();
        fieldAttrAutoCompleteInteractKey = ($("input[name^='" + fieldName + "-interactKey']").val() == undefined) ? false : $("input[name^='" + fieldName + "-interactKey']").val();
        fieldAttrAutoCompleteInteractField = ($("input[name^='" + fieldName + "-interactField']").val() == undefined) ? false : $("input[name^='" + fieldName + "-interactField']").val();

        fieldAttrAutoCompleteOptsDetails = $("input[name^='" + fieldName + "-details']").val();

        fieldInteractValue = "";

        fieldBaseName = fieldName.replace("_ac_","")

        objFieldHide = $("input[name^='" + fieldBaseName + "']");

        fieldAttrAutoCompleteInteractValue = "";

        //alert("oie");
        //console.log(fieldAttrAutoCompleteInteractField);

        if (fieldAttrAutoCompleteInteractField != "false" && fieldAttrAutoCompleteInteractField) {

            fieldShowAutoComplete.attr({"disabled":"disabled"});
            //console.log($("input[name='" + fieldAttrAutoCompleteInteractField + "']"));
            //console.log($("input[name='" + fieldAttrAutoCompleteInteractField + "']"));

            $("input[name='" + fieldAttrAutoCompleteInteractField + "']").bind("change", function(){

                fieldShowAutoComplete.removeAttr("disabled");
                //fieldShowAutoComplete.val("");

                fieldShowAutoComplete.focus();

                return false;

            });

            valueFieldInteract = $("input[name='" + fieldAttrAutoCompleteInteractField + "']").val();
            //console.log(valueFieldInteract.match(/\S/));

            if (valueFieldInteract.match(/\S/)) {
                $("input[name='" + fieldAttrAutoCompleteInteractField + "']").trigger("change");
            }

        }

        data = baseUrlApp() + "/" + fieldAttrAutoCompleteApp + "/_ac/" + fieldAttrAutoCompleteForm + "/" + fieldAttrAutoCompleteField;

        if (fieldAttrAutoCompleteOptsDetails == "1") {
            $(fieldShowAutoComplete).after("&nbsp;<a class='_sm-ac-data" + fieldName + "-details' href='javascript:void(0)' alt='" + objFieldHide.val() + "," + objFieldHide.attr("name") + "," + fieldAttrAutoCompleteForm + "," + fieldAttrAutoCompleteIndex + "," + appOpened() + "'>Mais detalhes</a>");
            $("._sm-ac-data" + fieldName + "-details").click(function(){
                objAlt = $(this).attr("alt");
                objArrayAlt = objAlt.split(",");
                if (objArrayAlt[0].match(/\S/)) {
                    _sa_AutoCompleteLoadDetails(objArrayAlt[2], objArrayAlt[4], objArrayAlt[3], objArrayAlt[0]);
                }
            });
        }

        //$(fieldShowAutoComplete).parent().after("<div style='text-align:center;font-size:11px;'><p><b>Atenção:</b> O campo acima possui recurso para facilitar o preenchimento.</p></div>");

        $(fieldShowAutoComplete).autocomplete(data, {
            width: 335,
            selectFirst: true,
            multiple: false,
            cacheLength : 0,
            matchContains: true,
            extraParams: {
                q: function(c1) {
                    return c1;
                },
                f : fieldAttrAutoCompleteIndex,
                fi : fieldName,
                st : fieldAttrAutoCompleteSearchType,
                fd : fieldAttrAutoCompleteFormatData,
                ik : fieldAttrAutoCompleteInteractKey,
                iv : function() {
                    return $("input[name='" + fieldAttrAutoCompleteInteractField + "']").val();
                }
            },
            formatItem: function(row, i, max) {
                return row[1];
            },
            formatResult: function(data, value) {
                r = data[1];
                return r;
            }
        }).result(function(event, item) {

            objFieldAutoComplete = $("input[name='" + item[2] + "']");
            objFieldHide = $("input[name='" + item[2] + "']");

            objFieldAutoComplete.val(item[1]);
            objFieldHide.val(item[0]).trigger("change");

            altString = item[0] + "," + item[2] + "," + item[3] + "," + item[4] + "," + item[5];

            $(objFieldAutoComplete).parent().find("a[class='_sm-ac-data_ac_" + item[2] + "-details']").attr({"alt":altString});

        });

    })
}

var activeSession;
function setActiveSessionInterval() {

    $(document).ready(function () {

        $("a[class*='tipsy-']").click(function(){
            $('.tipsy:last').remove();
        });

        //Increment the idle time counter every minute.
        var idleInterval = setInterval("timerIncrement()", 1000); // 1 minute 

        //Zero the idle timer on mouse movement.
        $(this).mousemove(function (e) {
            idleTime = 0;
        });

        $(this).keypress(function (e) {
            idleTime = 0;
        });

    });

}
setActiveSessionInterval();

idleTime = 0;
function timerIncrement() {
    idleTime = idleTime + 1;
    if (idleTime >= 3) { // 20 segundos = 1 minuto
        _sa_check_LockActiveSession();
        idleTime = 0;
    }
}
/*
function _sa_check_DirectLockActiveSession() {
    return 1;
}
*/

var rx;
function _sa_check_DirectLockActiveSession() {

    windowOpenedSessionCheck = $("input[name='sessionBlockWindow']").val();

    // check se a sessão do usuário está ativa.
    //console.log("Checa se a sessão está ativa");
    //objTimer.val("0");

    if (windowOpenedSessionCheck != 0) {
        return false;
    }

    urlBase = $("#sa-baseurl").val();
    wUrl = urlBase + "accounts/active/session";

    $.ajaxSetup({async: false, beforeSend: function(){}, complete: function(){}});

    ret = "";
    r = "";

    ret = $.post(wUrl,{callcon:"true"},function(r){

        rx = r;
        this.rtatus=r.status;

        if (r.status == 0) {

            $("input[name='sessionBlockWindow']").val("1");
            blockModel = new Boxy(r.login, {title: "Identificação necessária", closeable: false, modal: true, unloadOnHide: true});

            objContent = blockModel.getContent();
            objContent.find("input[name='login[EMAIL]']").val($("#sa-endaccess").val());
            window.clearInterval(activeSession);

            objContent.find("input[name='login[SENHA]']").focus();
            objContent.find("#imageBackSite a").tipsy({html: true, gravity: 'n'});
            blockModel.center();

            objContent.find("._sm_trigger_cancel").unbind("click").click(function(){
                blockModel.hide();
                window.location = urlBase;
            });

            objContent.find("._sm_trigger_submit").unbind("click").click(function(){
                $(this).closest('form').submit();
            });

            objContent.find("form[name='form_login']").submit(function(){

                objButton = $("form[name='form_login']").find("input[type='submit']");
                objButton.attr({"disabled":"disabled"}).val("Aguarde...");
                objSerialize = $(this).serialize();
                wUrlLink = urlBase + "accounts/login";
                objContent.find("#show-errors").hide();

                $.post(wUrlLink, objSerialize, function(callback){

                    if (callback.type == "error") {

                        objButton.removeAttr("disabled").val("Acessar");
                        //$("#show-errors").html(callback.msg).show();
                        _sa_showMessage("Atenção",callback.msg);

                    } else {

                        objAppOpened = $("#sa-appschema").val();

                        if (objAppOpened != undefined) {

                            url = urlBase + "appset";
                            //link = window.location.href;

                            blockModel.hide();

                            $.post(url,{
                                schema: objAppOpened
                            },function(data){

                                if (data){
                                    //window.location = link;
                                    $("input[name='IntSessionTimer']").val("0");
                                    $("input[name='sessionBlockWindow']").val("0");
                                    _sa_set_baseDataAtual();
                                    setActiveSessionInterval();
                                    setAjaxSend();

                                    if (confirm('Autenticação realizada com sucesso. \n\nSerá necessário atualizar a página para exibir as informações atualizadas.')) {
                                        window.location.reload();
                                    }

                                } else {
                                    alert('Não foi possível prosseguir com a operação. Você será enviado a area de validação de acesso.');
                                    window.location = urlBase;
                                }

                            });
                        } else {
                            window.location.reload();
                        }
                    }
                }, "json");

                return false;

            });

            return false;

        } else {
            _sa_set_baseDataAtual();
            setAjaxSend();
        }

        return false;

    }, "json");

    if (rx != undefined) {
        return rx.status;
    } else {
        return false;
    }

}

function _sa_set_baseDataAtual() {
    baseDataAtual = new Date();
    $("input[name='DateTimerSession']").val(baseDataAtual);
}

function _sa_check_LockActiveSession() {

    objTimer = $("input[name='IntSessionTimer']");
    intTimer = new Number(objTimer.val());
    v = intTimer.valueOf();
    objTimer.val(intTimer+1);
    objActual = objTimer.val();
    windowOpenedSessionCheck = $("input[name='sessionBlockWindow']").val();

    if (objActual == 4 && windowOpenedSessionCheck == 0) {
        objTimer.val("0");

        urlBase = $("#sa-baseurl").val();
        wUrl = urlBase + "accounts/active/session";
        $.post(wUrl,{callcon:"true"},function(r){

            if (r.status == 0) {

                blockModel = new Boxy(r.login, {title: "Identificação necessária", closeable: false, modal: true, unloadOnHide: true});
                $("input[name='sessionBlockWindow']").val("1");
                objContent = blockModel.getContent();
                objContent.find("input[name='login[EMAIL]']").val($("#sa-endaccess").val());
                window.clearInterval(activeSession);

                objContent.find("input[name='login[SENHA]']").focus();
                objContent.find("#imageBackSite a").tipsy({html: true, gravity: 'n'});
                blockModel.center();

                objContent.find("._sm_trigger_cancel").unbind("click").click(function(){
                    blockModel.hide();
                });

                objContent.find("._sm_trigger_submit").unbind("click").click(function(){
                    $(this).closest('form').submit();
                });

                objContent.find("form[name='form_login']").submit(function(){

                    objButton = $("form[name='form_login']").find("input[type='submit']");
                    objButton.attr({"disabled":"disabled"}).val("Aguarde...");
                    objSerialize = $(this).serialize();
                    wUrlLink = urlBase + "accounts/login";
                    objContent.find("#show-errors").hide();

                    $.post(wUrlLink, objSerialize, function(callback){

                        if (callback.type == "error") {
                            objButton.removeAttr("disabled").val("Acessar");
                            //$("#show-errors").html(callback.msg).show();
                            _sa_showMessage("Atenção",callback.msg);
                        } else {

                            blockModel.hide();

                            objAppOpened = $("#sa-appschema").val();

                            if (objAppOpened != undefined) {

                                url = urlBase + "appset";
                                //link = window.location.href;

                                $.post(url,{
                                    schema: objAppOpened
                                },function(data){
                                    if (data){
                                        //window.location = link;
                                        $("input[name='IntSessionTimer']").val("0");
                                        $("input[name='sessionBlockWindow']").val("0");
                                        setActiveSessionInterval();
                                    } else {
                                        alert('Não foi possível prosseguir com a operação. Você será enviado a area de validação de acesso.');
                                        window.location = urlBase;
                                    }
                                });
                            } else {
                                window.location.reload();
                            }
                        }
                    }, "json");

                    return false;

                });

                return false;

            }

            return false;

        }, "json");

    }

    if (objActual == 4) {
        $("input[name='IntSessionTimer']").val("0");
    }

}

function setAjaxSend() {

    $.ajaxSetup({
        global: true,
        async: true,
        beforeSend: function() {

            diffMinutes = 5;

            //$(".loading-starts").show();
            //$(".loading-end").hide();

            $(".status-activity-loading").show();
            $(".status-activity-success").hide();

            objInputDateCheck = $("input[name='DateTimerSession']").val();
            windowOpenedSessionCheck = $("input[name='sessionBlockWindow']").val();

            objDateCr = new Date(objInputDateCheck);
            objDateBase = new Date();

            var timeDiff = Math.abs(objDateCr.getTime() - objDateBase.getTime());
            var timeSeconds = Math.round(timeDiff/1000);

            //console.log(objInputDateCheck);
            //console.log(objDateBase);
            //console.log(timeSeconds);
            if (timeSeconds > 50 && windowOpenedSessionCheck == 0) {
                bfs=_sa_check_DirectLockActiveSession();
                //console.log(bfs);
                //bfs=1;
            } else {
                bfs=1;
            }

            if (bfs == 1) {
                return true;
            } else {
                return false;
            }

        },
        complete: function(r) {

            rtext = r.responseText;
            if (rtext.match(/<html id="smart-ui">/)) {
                //console.log(rtext);
                alert("A sua sessão expirou, será necessário você autenticar novamente no ambiente.");
                window.location.reload();
            }

            //$(".loading-starts").hide();
            //$(".loading-end").show();
            $(".status-activity-loading").hide();
            $(".status-activity-success").show();

            //var iframe = document.getElementById("frameView");
            //$(iframe).trigger("load");
            /*
            height = $(iframe).find("body").height();

            $("#frameView").css({
                height: window.parent.$("#frameView").height()
            });*/

        }
    });

}

setAjaxSend();

function setPortraitArea() {

    if (window.innerWidth < 1024) {
        $("#direita").hide();
        $(".view-area-portrait").css({width: '765'});
    } else {
        $("#direita").show();
        $(".view-area-portrait").css({width: '1010'});
    }

    if(/Android|webOS|iPhone|iPad|BlackBerry/i.test(navigator.userAgent) ) {
        $("#direita").hide();
        $(".view-area-portrait").css({width: '775'});
    }

}

function checkPortraitArea() {
    setInterval('setPortraitArea()', 1000);
}

function _sdk_chart_plot(objThis) {
    objJDContent = JSON.parse($("input[name='" + objThis + "-jd']").val());
    _sdk_chart_make(false, objJDContent, false, false, false, objThis);
}

function _sdk_chart_make(objForm, jsonReturnData, typeArea, b, dataLabels, areaName) {

    if (dataLabels == undefined) {
        dataLabels = true;
    }

    //defaultData = {}

    if (typeArea == undefined || typeArea == false) {

        if (b === true) {
            $("#" + jsonReturnData.chart.renderTo).css({
                "width":"900px",
                "height":"430px",
                "margin":"10px"
            });
            objContent = b.getContent();
            objContent.find("#visualizarDados").css({
                "margin-left":"219px"
            });
            b.center();
        }

    } else {
        switch(typeArea) {
            case "share":
                $("#" + jsonReturnData.chart.renderTo).css({
                    "width":"600px",
                    "height":"430px",
                    "margin":"10px"
                });
                b.center();
                break;
        }

    }

    if (objForm != false) {
        showType = objForm.find("select[name='showtype']").val();
        cType = objForm.find("select[name='tipo']").val();
    } else {
        showType = "number";
        cType = (jsonReturnData.chart.defaultSeriesType != undefined) ? jsonReturnData.chart.defaultSeriesType : "pie" ;
    }

    switch (showType) {
        case "number":

            defaultData = {
                tooltip: {
                    formatter: function() {
                        stra = "";
                        objThis = this;
                        cType = $("input[name='" + areaName + "-type']").val();
                        switch(cType){
                            case "pie":
                                stra = '<b>'+ objThis.point.x +'</b> (' + objThis.point.l + ')';
                                break;
                            case "bar":
                                console.log(objThis);
                                stra = '<b>'+ objThis.point.config[0] +'</b> (' + objThis.point.config[(objThis.point.config.length-1)] + ')';
                                break;
                            case "line":
                                stra = '<b>'+ objThis.point.category +'</b> (' + objThis.y + ')';
                                break;
                        }

                        return stra;
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: dataLabels,
                            formatter: function() {
                                var ptage = this.y;
                                return '<b>'+ this.point.name +'</b> ('+ ptage +')';
                            }
                        }
                    },
                    bar: {
                        showInLegend: dataLabels,
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                exporting: {
                    enabled: true
                }
            }
        break;
        case "percentage":
            defaultData = {
                tooltip: {
                    formatter: function() {

                        ptag = this.percentage;
                        stra = "";
                        switch(cType){
                            case "pie":
                                stra = '<b>'+ this.point.name +'</b> ' + ptag.toFixed(2) + '%';
                                break;
                            case "bar":
                                stra = '<b>'+ this.x + '</b> (' + this.y + ')';
                                break;
                            case "line":
                                stra = '<b>'+ this.point.name +'</b> (' + this.y + ')';
                                break;
                        }

                        return stra;
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            formatter: function() {

                                var ptag = this.percentage;

                                stra = "";
                                //alert(cType);
                                switch(cType){
                                    case "pie":
                                        stra = '<b>'+ this.point.name +'</b> ' + ptag.toFixed(2) + '%';
                                        break;
                                    case "bar":
                                        stra = '<b>'+ this.x + '</b> (' + this.y + ')';
                                        break;
                                    case "line":
                                        stra = '<b>'+ this.point.name +'</b> (' + this.y + ')';
                                        break;
                                }

                                return stra;
                            }
                        }
                    }
                }
            }
            break;
    }

    data = jsonReturnData;

    dataAttrs = $.extend(defaultData, data);

    chart1 = new Highcharts.Chart(dataAttrs);

    return chart1;

}

function call_helpme() {
    $("#opt-helpme").trigger("click");
}

function call_navegacao() {
    $("#opt-favoritos").trigger("click");
}

function load_alerts_contents(element, callback, moa) {
    if (moa == undefined) {
        $(element).html(callback.content);
    } else {
        $(element).find("li").last().after(callback.content);
    }

    if (callback.totalNaoLido > 0) {
        totalNaoLidoEnd = (callback.totalNaoLido > 10) ? "10+" : callback.totalNaoLido;
        $(".indicator-alert-layer").text(totalNaoLidoEnd);
        $(".indicator-alert-layer").show();
    } else {
        $(".indicator-alert-layer").hide();
    }

    _sa_formatButtons();
    _sa_formatTipsy();

    _sa_notifyAlertSidebar(element);

    if (callback.totalRegistros > (callback.limitRecordsShow-1)) {
        $(element).find(".more-alerts").unbind("click").click(function(){
            load_alerts(1, element, 1);
        });
    } else {
        $(element).find(".more-alerts").unbind("click").remove();
    }


}

function load_alerts(t, element, moa) {

    featureNotify = $("input[name='sa-featuresNotify']").val();

    if (featureNotify == 1) {

        if (t==0) {

            $(".sm-notify-alerts-msgs").each(function(index, element){

                wUrlAlerts = _sa_baseUrl() + "tools/notify/alerts/msgs";
                $.getJSON(wUrlAlerts, function(callback){

                    load_alerts_contents(element, callback);

                });

            });

        } else {

            wUrlAlerts = _sa_baseUrl() + "tools/notify/alerts/msgs";
            bdate = $("input[name^='notify-sidebar-datac-']").last().val();

            $.post(wUrlAlerts,{bdate: bdate}, function(callback){
                load_alerts_contents(element, callback, moa);
            }, "json");

        }

    }

}

function _sa_notifyAlertSidebar(c) {

    $(c).find(".sm-notify-alert-hide").unbind("click").click(function(){

        objThis = $(this);
        idAlert = $(this).attr("alt");

        $('.tipsy:last').remove();

        wUrlAlertHide = _sa_baseUrl() + "tools/notify/alerts/msgs/cancel";
        $.post(wUrlAlertHide, {id: idAlert}, function(callback){

            if (callback.type == "success") {
                $.jGrowl(callback.msg,{
                    header: "Atenção"
                });
                objThis.parent().parent().parent().hide();
            }

        }, "json");

    });

    $(c).find(".sm-notify-alert-open").unbind("click").click(function(){

        idAlertNotify = $(this).attr("alt");

        notifyApp = $(c).find("input[name='notify-sidebar-app-" + idAlertNotify + "']").val();
        notifyMsg = $(c).find("input[name='notify-sidebar-msg-" + idAlertNotify + "']").val();
        notifyDate = $(c).find("input[name='notify-sidebar-data-" + idAlertNotify + "']").val();
        notifyStatus = $(c).find("input[name='notify-sidebar-status-" + idAlertNotify + "']").val();

        var strContent = "<div class='sm-notify-alerts-msgs-detail sm-width-800'><div class='sm-padding sm-font-12'><b>Aplicativo:</b>&nbsp;" + notifyApp + "<br><b>Data registro:</b>" + notifyDate + " <br><br><b>Mensagem:</b><br>" + notifyMsg + "</div></div>";
        var bo = new Boxy(strContent, {title: "<b>DETALHES</b>", modal: true, unloadOnHide: true});

        if (notifyStatus == 0) {

            wUrlAlertRead = _sa_baseUrl() + "tools/notify/alerts/msgs/markread";
            $.post(wUrlAlertRead, {id: idAlertNotify}, function(callback){

                if (callback.type == "success") {
                    load_alerts();
                }

            }, "json");

        }

    });

}

function _sa_appOpen(schema, href) {

    altValue = schema;
    url = $("input[name='sa-baseurl']").val() + "appset";
    link = href;

    $.post(url,{
        schema: altValue
    },function(data){
        if (data){

            window.location = link;

        } else {
            alert('Não foi possível prosseguir com a operação. tente novamente.');
        }
    });

    return false;

}

$("document").ready(function(){

    objBaseAction = $(".contentApp");
    _sa_setDesignButtons();
    _sa_desactiveButtonSubmitAfterAction(objBaseAction);
    _sa_loadComponents(objBaseAction);
    _sa_AutoCompleteLoad();

    setPortraitArea();
    checkPortraitArea();

    if ($(".bartop").val() != undefined) {
        $(".bartop").detachit();
    }

    $(".button-profile").button({ icons: { primary: "ui-icon-person", secondary: "ui-icon-triangle-1-s"} });

    if ($.browser.msie) {
        window.location = _sa_baseUrl() + "site/browsers/ie/notsupported";
    }

    // fix scrollbars on menu
    $('aside .sec-content-wrapper > ul').bind('mousewheel DOMMouseScroll', function(e) {
        var e0 = e.originalEvent, delta = e0.wheelDelta || -e0.detail;
        this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
        e.preventDefault();
    });

    $(".graphs-list").find("select").fancySelect();
    $(".graphs-list").find("select").find("option:first-child").attr("selected", true).trigger("change");

    // trigger form for show optional fields
   objForm = $(".contentApp").find("form");
   _sa_ContentFormRequiredFields(objForm);

    $('.contentApp').ajaxComplete(function(event,request, settings){
        validJson = _validate_json(request.responseText);
        if (validJson) {
            jQueryJson = jQuery.parseJSON(request.responseText);
            if (jQueryJson != null && jQueryJson.type == "error") {
                if (jQueryJson.data != undefined) {
                    $.jGrowl(jQueryJson.data,{
                        header: "Atenção"
                    });

                    // hide last boxy created
                    objBoxyBlackout = $(".boxy-modal-blackout");
                    objBoxyContent = $(".boxy-wrapper");
                    boxyLength = (objBoxyBlackout.length >= 1) ? objBoxyBlackout.length-1 : 0;
                    //if (boxyLength >= 1) {
                    objBoxyBlackout.each(function(index, element){
                        if (boxyLength == index) {
                            zBlackoutIndex = new Number($(element).css("z-index"));
                            objBoxyContent.each(function(index2, element2){
                                zContentIndex = new Number($(element2).css("z-index"));
                                if (zContentIndex == zBlackoutIndex+1) {
                                    $(element2).remove();
                                    $(element).remove();
                                }
                            });
                        }
                    });

                    //}

                }
            }
        }
    });

    $(".show-profile-opts").unbind("click");
    $(".show-profile-opts").toggle(function(){
        wUrlProfileOpts = _sa_baseUrl() + "perfil/options/usuario";
        $.get(wUrlProfileOpts, function(dataOptions){
            $(".bartop-menu").html(dataOptions);
            $(".bartop-menu").fadeIn();
            window.scrollTo(0,0);
        });
    }, function(){
        window.scrollTo(0,0);
        $(".bartop-menu").fadeOut();
    });

    var arrowimages={down:['downarrowclass', _sa_baseUrl() + "/libs/icons/down.gif", 23], right:['rightarrowclass', _sa_baseUrl() + "/libs/icons/right.gif"]}
    jqueryslidemenu.buildmenu("smMenu", arrowimages);

    $(".contentApp").find("form")

    url = location.href;
    baseURL = $("#sa-baseurl").val();

    var opts = {
        baseUrl : baseURL,
        appName : $("#sa-appname").val(),
        appParams : $("#sa-appparams").val(),
        appPathInfo : $("#sa-apppathinfo").val(),
        appFavoriteStatus : $("#sa-appfavoritestatus").val(),
        saBaseUrl : $("#sa-baseurl").val()
    };
    /*
    $("input[type='text']").each(function(){

        $(this).focus(function(){
            $(this).css({
                "background-color":"#fffaed"
            });
        });

        $(this).blur(function(){
            $(this).css({
                "background-color":"white"
            });
        });

    });*/

    //$(".tipsy-w").tipsy({gravity:"w", html: true});
    _sa_formatButtons();

    $(".sm-header-app").each(function(){

        objAlt = $(this).attr("alt");
        objValArea = $("input[name='uiSchemaBg-" + objAlt + "']").val();

        if (objValArea != undefined) {

            $(this).css({"background-color":$("input[name='uiSchemaBg-" + objAlt + "']").val(), "color": "white"});
            $(this).find("h1, h2, h3, p, small").css({"color":$("input[name='uiSchemaTxt-" + objAlt + "']").val()});
        }

    });

    $(".sm-header-app .subinfo").smartTools();

    $(".listAreaBlocks li a, .icon-avatar-profile").tipsy({
        gravity:"s",
        html:true
    });

    qntdAppsInstalados = 0;
    $("li[class*='_saAppArea']").each(function(){
        /*
        widthBlock = $(this).width();
        objAppendApps = jQuery(this);
        console.log("oie");
        $(this).find("img").each(function(){

            valueTitle = $(this).attr("title");

            if (valueTitle != "" && valueTitle != undefined) {

                schema = $(this).attr("title");
                layer = document.createElement("div");
                offSet = jQuery(this).position();

                jQuery(layer).attr({
                    "id":"tiplayer-notify-" + schema
                });

                $(layer).addClass("tip-schema").text(schema);

                objAppendApps.append(layer);

            }
        });*/
        qntdAppsInstalados++;
    });

    /**
     * Dashboard / Design
     */
    $("#nav-points").each(function(){

        pnBg = $("input[name='uiDashboard-pointnav-bg']").val();
        pnLabelBg = $("input[name='uiDashboard-pointnav-label']").val();
        pnLabelText = $("input[name='uiDashboard-pointnav-label-text']").val();

        appsBg = $("input[name='uiDashboard-apps-bg']").val();
        appsLabelBg = $("input[name='uiDashboard-apps-label']").val();
        appsLabelText = $("input[name='uiDashboard-apps-label-text']").val();

        prefsBg = $("input[name='uiDashboard-settings-bg']").val();
        prefsLabelBg = $("input[name='uiDashboard-settings-label']").val();
        prefsLabelText = $("input[name='uiDashboard-settings-label-text']").val();

        $(".pn-sec").css({"background-color":pnBg});
        $(".pn-sec").find(".applabel").css({"background-color":pnLabelBg, "color": pnLabelText});

        $(".apps-sec").css({"background-color":appsBg});
        $(".apps-sec").find(".applabel").css({"background-color": appsLabelBg, "color": appsLabelText});

        $(".prefs-sec").css({"background-color":prefsBg});
        $(".prefs-sec").find(".applabel").css({"background-color": prefsLabelBg, "color": prefsLabelText});

    });

    $("div[class='subinfo']").each(function(){
        widthBlock = $(this).width();
        objAppend = jQuery(this);
        $(this).find("img").each(function(){
            valueTitle = $(this).attr("title");
            if (valueTitle != "" && valueTitle != undefined) {
                schema = $(this).attr("title");
                layer = document.createElement("div");
                offSet = jQuery(this).position();
                jQuery(layer).attr({
                    "id":"tiplayer-notify-" + schema
                    });
                $(layer).addClass("tip-schema-contentapp").text(schema);
                objAppend.append(layer);
            }
        });
    });

    /*
     * Options Dashboard
     *
     * 1. Navigator
     */
    $("#opt-favoritos").bind("click", function(){

        $.getScript(opts.baseUrl + 'libs/smJS/options/fav.js',function(){

            $.get(opts.baseUrl + "preferencias/favoritos/open",function(data){

                b = new Boxy(data,{
                    title:"&nbsp;",
                    unloadOnHide:true,
                    modal: true
                });

                _sa_formatButtons();
                _sa_setDesignButtons();

                $("#window-favoritos-header").find(".list-options").find("li").find("a").click(function(){

                    _sa_favoritosAbaDisable();

                    $(this).parent().addClass("selected");
                    objWindowFavoritosList = $("#window-favoritos-listar");

                    _sa_favoritosLoadingCenter();

                    wUrl = $(this).attr("href");

                    $.getJSON(wUrl,function(data){

                        _sa_favoritosClearCenter();
                        _sa_favoritosSetDataCenter(data.content);

                        switch(data.type) {
                            case "grupos":

                                dataContent = $("#list-favoritos-grupos");

                                _sa_opt_favoritosListaEditar(dataContent);
                                _sa_opt_favoritosListaApagar(dataContent);
                                _sa_opt_favoritosListaAbrir(dataContent);
                                _sa_setDesignButtons();

                                objFormGrupos = $("#opt-favorito-adicionar-grupo");
                                objFormGrupos.find(".b_adicionar").click(function(){
                                    nomeGrupo = objFormGrupos.find("input[name='nome']").val();
                                    if (!nomeGrupo.match(/\S/)) {
                                        $.jGrowl("Informe o nome do grupo.",{
                                            header: "Atenção"
                                        });
                                    } else {
                                        _sa_favoritosGrupos(objFormGrupos);
                                    }
                                });

                                _sa_formatButtons();

                            break;
                            case "aplicativos":
                                dataContent = $("#list-favoritos-aplicativos");
                                _sa_opt_favoritosAplicativosAbrir(dataContent);

                                _sa_formatButtons();

                            break;
                        }

                        _sa_opt_favoritosEditar(objWindowFavoritosList);
                        _sa_opt_favoritosApagar();

                    });

                    return false;

                });

                $("a.opt-favoritos-apresentacao").trigger("click");

                _sa_opt_favoritosEditar();
                _sa_opt_favoritosApagar();

            });

        });

        return false;

    });

    /*
     * Options Dashboard
     *
     * 2. HelpMe!
     */

    $("#opt-helpme").bind("click", function(){
        objClick = $(this);
        bHelpMe = new Boxy("<div>Aguarde ... carregando ...</div>",{
            unloadOnHide:true, modal: true, center: true, title: "HelpMe!", actuator: objClick
        });

        $.get(opts.baseUrl + "opts/help/open",function(data){

            bHelpMe.setContent(data);
            bHelpMe.center();
            objectContent = bHelpMe.getContent();
            $(objectContent).find(".helpme-left").find("a").click(function(){
                $(objectContent).find(".helpme-left").find("a").css({"font-weight":"normal", "color": "gray"});
                $(this).css({"font-weight":"bold", "color": "#c00000"});
                objRel = $(this).attr("rel");
                objectWindow = $(objectContent).find(".helpme-content");
                switch(objRel) {
                    case "new":
                        _sa_helpme_new(objectWindow, bHelpMe);
                    break;
                    case "list-abertos":
                        _sa_helpme_list(objectWindow, bHelpMe, 0);
                    break;
                    case "list-fechados":
                        _sa_helpme_list(objectWindow, bHelpMe, 1);
                    break;
                }
            });
            $(objectContent).find(".helpme-left").find("a:eq(0)").trigger("click");

        });
    });

    /*
     * Options GDE
     *
     * 2. Graphs!
     */
    $("#opt-graphs").bind("click",function(){
        $.getScript(opts.baseUrl + 'libs/smJS/options/graphs.js',function(){
            $.get(opts.baseUrl + "opts/graphs/open",function(data){

                b = new Boxy(data,{
                    unloadOnHide:true, modal: true, center: true, title: "Gráficos & Dados"
                });

                objectContent = b.getContent();
                $(objectContent).find(".graphs-left").find("a").click(function(){

                    $(objectContent).find(".graphs-left").find("a").css({"font-weight":"normal", "color": "gray"});
                    $(this).css({"font-weight":"bold", "color": "#c00000"});
                    objRel = $(this).attr("rel");
                    objectWindow = $(objectContent).find(".graphs-content");
                    switch(objRel) {
                        case "inicio":
                            _sa_graphs_begin(objectWindow, b);
                        break;
                        case "shared":
                            _sa_graphs_shared(objectWindow, b);
                        break;
                    }
                });
                $(objectContent).find(".graphs-left").find("a:eq(0)").trigger("click");

            });
        });
    });

    /**
     * Options Preferences / User
     *
     * 3. Preferences
     */

    $("#opt-preferences").bind("click", function(){

        objClick = $(this);
        bPreferences = new Boxy("<div>Aguarde ... carregando ...</div>",{
            unloadOnHide:true, modal: true, center: true, title: "&nbsp;", closeable: false, actuator: objClick
        });

        $.get(opts.baseUrl + "perfil/preferences/list",function(data){
            bPreferences.setContent(data);
            bPreferences.center();

            objContent = bPreferences.getContent();
            objContent.find("form").submit(function(){

                objSerialize = $(this).serialize();

                $.post(opts.baseUrl + "perfil/preferences/save",objSerialize, function(data){

                    $.jGrowl(data.msg,{
                        header: "Atenção"
                    });

                    if (data.type == "sucess") {
                        bPreferences.hide();
                    }

                }, "json");

            });

            objContent.find(".savePreferences").unbind("click").click(function(){
                $(this).html("<b>AGUARDE ...</b>");
                objContent.find("form").trigger("submit");
            });

        });


    });

    // when click execute a call to set the app schema.
    $(".sm_appset").click(function(){

        altValue = $(this).attr("alt");
        url = $("input[name='sa-baseurl']").val() + "appset";
        link = $(this).attr("href");

        //_sa_appOpen(altValue, link);

        $.post(url,{
            schema: altValue
        },function(data){
            if (data){

                window.location = link;

            } else {
                alert('Não foi possível prosseguir com a operação. tente novamente.');
            }
        });

        return false;

    });

    // se o usuário tem apenas uma aplicação na plataforma, carrega esta aplicação automaticamente.
    $("#loadingData").show();

    if ($("#sa-autoLoadUniqueApp").val() == "true") {
        if (qntdAppsInstalados == 1) {
            $("li[class*='_saAppArea']:eq(0)").find("a").trigger("click");
        } else {
            $(".area-content").show();
            $(".sidebar").show();
            $("#loadingData").hide();
        }
    } else {
        $(".area-content").show();
        $(".sidebar").show();
        $("#loadingData").hide();
    }

    $(".noInstalled").find("div").find("div").attr({"title":"Abra o aplicativo para concluir a instalação."}).tipsy({gravity:'s'});
    $(".noInstalled").find("div").find("div").html("<img src='" + _sa_baseUrl() + "/libs/imgs/mini_progressbar.png' align='left'>");

    load_alerts(0);

});

/*
 * Custom form search.
 *
 */
function customFormSearch(urlfields,formName) {
    wUrlFields = urlfields;
    $.post(wUrlFields,{"cached":$(this).attr("alt"),"js": true},function(data){
            bX2678 = new Boxy(data, {title: "Personalize sua busca",closeText:"[Fechar janela]"});
            $(bX2678.getContent()).find(".listFieldsOptions").find("input[type='checkbox']").each(function(){
                    idOption = "#customsearch-" + $(this).attr("alt");
                    if ($(idOption).find("input").val() != undefined) { $(this).attr("checked",true); }
                    $(this).click(function(){
                            statusCheckbox = $(this).is(":checked");

                            if (statusCheckbox) {
                                    htmlInsertOption = "<div id='customsearch-" + $(this).val() + "'><b>" + $(this).attr("title") + ":</b><br><input name=\"" + $(this).val() +  "\" type=\"text\" value=\"\"></div>";
                                    $("#objBefore").before(htmlInsertOption);
                                    $("#customsearch-" + $(this).val()).find("input").css({"width":"205px"}).focus(function(){
                                            $(this).css({"background-color":"#fffaed"});
                                    }).blur(function(){
                                            $(this).css({"background-color":"#fff"});
                                    });
                            } else {
                                    $("#customsearch-" + $(this).val()).remove();
                            }
                    });
            });
    });

    return false;
}

/*
 * CRUD Mask Fields, for form.
 */
(function($){
   $.fn.sm_mask = function() {
       $(this).find("input[id$='-mask']").each(function(){
          vmask = $(this).attr("title");
          $(this).mask(vmask);
       });
   }
})(jQuery);
