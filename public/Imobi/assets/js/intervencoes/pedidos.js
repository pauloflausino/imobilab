
function formPedidosInsert(t) {

    objForm = $("form[name^='pedidos_insert']");

    objForm.find("input[name='pep_nivel_3']").parent().before("<br><div><small><p align='center'><a href='javascript:;;' class='sm-buttons-flat-gray add-new-pep'>ADICIONAR PEP &nbsp;<i class='icon-plus'></i></a></p></small></div><br>");

    objForm.find("input[name='pep_nivel_3']").attr({"name" : "pep_nivel_3[]"});
    objForm.find("select[name='tipo_pep']").attr({"name" : "tipo_pep[]"});
    objForm.find("input[name='id_cpom']").attr({"name" : "id_cpom[]"});
    objForm.find("select[name='tipo_cpom']").attr({"name" : "tipo_cpom[]"});

    cloneArea = "";

    t.window.center();

    $(".add-new-pep").unbind("click").click(function(){
        objClone = "<hr><div class='field-required f-field-text'> " + objForm.find("input[name='pep_nivel_3[]']:eq(0)").parent().html() + "</div>" + "<div class='field-required f-field-text'>" + objForm.find("select[name='tipo_pep[]']:eq(0)").parent().html() + "</div><div class='field-required f-field-text'>" + objForm.find("input[name='id_cpom[]']:eq(0)").parent().html() + "</div><div  class='field-required f-field-text'>" + objForm.find("select[name='tipo_cpom[]']:eq(0)").parent().html() + "</div>";
        objForm.find("select[name='tipo_cpom[]']:eq(0)").parent().after(objClone);
        t.window.center();
        formatTipsy();
    });

}

function formPedidosPost(t) {
    qntdRecords = t.req.arrayData.length;
    stringMsgs = "";
    for(i=0; i < qntdRecords; i++) {
        objRecord = t.req.arrayData[i];
        statusReq = objRecord.request.status;
        if (statusReq == "error") {
            lengthErrors = objRecord.request.msgReturn.length;
            stringMsgs += "<div><h2 class='sm-plnb'> Registro  (" + (i+1) + ")</h2>";
            for(ix=0; ix < lengthErrors; ix++) {
                stringMsgs += "<p>" + objRecord.request.msgReturn[ix] + "</p>";
            }
            stringMsgs + "</div>";
        } else {
            stringMsgs += "<div><h2 class='sm-plnb'> Registro  (" + (i+1) + ")</h2>";
            stringMsgs += "<p>" + objRecord.request.msgReturn + "</p>";
            stringMsgs + "</div>";
        }
    }
    t.window.setContent("<div class='sm-width-600 sm-padding' style='max-height:600px;overflow:auto;'>" + stringMsgs + "<br><p align='center' class='sm-padding'><a href='javascript:;;' onclick='Boxy.get(this).hide();' class='sm-buttons-flat-blue'>Fechar janela</a></p><br></div>");
    t.window.center();
}

function formPedidos(t) {

	objForm = $("form[name^='pedidos_edit']");

    objForm.find("input[name='id_pmts']").parent().prepend("<br><span class='sm-padding sm-color-gray'><b>DETALHES PEDIDO</b></span><br><br>");
    objForm.find("input[name='pep_nivel_3']").parent().prepend("<br><span class='sm-padding sm-color-gray'><b>DADOS CPOM</b></span><br><br>");

	objForm.find("input[type='text']").parent().show();

	t.window.center();

    formatTipsy();

	objContent = t.window.getContent();

	pedidoId = objContent.find("input[name='saTableFieldValue']").val();

    btnClose = t.window.getContent().find("a:contains('Cancelar')");

    btnClose.after(" | <a class='open-historico-pedido' href='javascript:;;'>Histórico</a>")

    btnClose.removeAttr("onclick").unbind("click").click(function(){
        Boxy.confirm("<h2>Notificação</h2><span class='tlf-cor-azul-padrao'>Tem certeza que deseja fechar a janela?</span>", function(){
            t.window.hide();
        });
        return false;
    });

    objContent.find(".open-historico-pedido").unbind("click").click(function(){

    	wUrlPedidoHistorico = baseUrlApp() + "/epmts/pedidos/historico/" + pedidoId;
    	bo = new Boxy("<div>Aguarde carregando ...</div>", {modal: true, unloadOnHide: true});

    	$.get(wUrlPedidoHistorico, function(callback){

    		bo.setContent(callback);
    		formatGrids();
    		bo.center();

    		bo.getContent().find(".pedidos-historico-detalhes-view").unbind("click").click(function(){

    			idDetalhe = $(this).attr("alt");
    			wUrlPedidoHistoricoDetalhe = baseUrlApp() + "/epmts/pedidos/historico/detalhes/" + idDetalhe;

		    	bod = new Boxy("<div>Aguarde carregando ...</div>", {modal: true, unloadOnHide: true});

		    	$.get(wUrlPedidoHistoricoDetalhe, function(callbackDetalhe){

		    		bod.setContent(callbackDetalhe);
		    		bod.center();

		    	});

    		});

    	});

    });



}