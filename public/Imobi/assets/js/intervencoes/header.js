function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

$(document).ready(function(){

	$(".opt-links-rapidos").unbind("click").click(function(){

		bo = new Boxy("<div class='paddingBox'>AGUARDE... CARREGANDO...</div>", {title: "Links Rápidos", modal: true, unloadOnHide: true});

		wUrlLoadLinks = baseUrlApp() + "/epmts/options/visualizar/links";
		$.get(wUrlLoadLinks, function(callback){
			bo.setContent(callback);
			formatGrids();
			bo.center();
		});

	});

	$(".toggle-associar-player-modalidade input[type='checkbox']").unbind("click").click(function(){
		isChecked = $(this).is(":checked");
		objValue = $(this).val();
		if (!isChecked) {

			wUrlAssociarModalidade = baseUrlApp() + "/epmts/fp/players/raquisicao/remover/" + objValue;
			$.post(wUrlAssociarModalidade, {val:objValue}, function(callback){

				$.jGrowl(callback.msg, {
                    header: "Atenção"
                });

				if (callback.type == "error") {
					// check the checkbox again
				}

			}, "json");

		} else {

			wUrlAssociarModalidade = baseUrlApp() + "/epmts/fp/players/raquisicao/registrar/" + objValue;
			$.post(wUrlAssociarModalidade, {val:objValue}, function(callback){

				$.jGrowl(callback.msg, {
                    header: "Atenção"
                });

				if (callback.type == "error") {
					// uncheck the checkbox again
				}

			}, "json");

		}
	});

	breadcrumb = $(".breadcrumb").html();
	if (breadcrumb == undefined) {
		titleArea = capitalizeFirstLetter($(".contentApp #showAlert:eq(0)").find("h2").text().toLowerCase());
		$(".contentApp #showAlert:eq(0)").before("<ol class='breadcrumb'><li><a href='" + baseUrlApp() + "/epmts'>Início</a></li><li class='active'>" + titleArea + "</li></ol>");
		textArea = capitalizeFirstLetter($(".contentApp #showAlert a").text().toLowerCase());
		$(".contentApp #showAlert a").addClass("btn-tlf").css({color:"white"}).text(textArea);
		$(".contentApp #epmts-custom-search").find("input[type='submit']").val("Pesquisar");
	}

	$(".opt-telefones-uteis").unbind("click").click(function(){

		bo = new Boxy("<div class='paddingBox'>AGUARDE... CARREGANDO...</div>", {title: "Telefones Úteis", modal: true, unloadOnHide: true});

		wUrlLoadTelefones = baseUrlApp() + "/epmts/options/visualizar/telefones";
		$.get(wUrlLoadTelefones, function(callback){
			bo.setContent(callback);
			formatGrids();
			bo.center();
		});

	});

	$(".linkMenu").click(function(event){
		$("#modalMenu").removeClass('fade').css('display','block');
		$(".tituloMenu").removeClass('actionMenu');
		event.preventDefault();
	});

	$(".fecharModalMenu").click(function(event){
		$("#modalMenu").addClass('fade').css('display','none');
		event.preventDefault();
	});

	$(".tituloMenu").hover(function(event){
		var conteudo = $(this).attr('alt');
		$(".tituloMenu").removeClass('actionMenu');
		$(".conteudoMenu").removeClass('conteudoActiveMenu');
		$(this).addClass('actionMenu');
		$('#container'+conteudo).addClass('conteudoActiveMenu');
		event.preventDefault();
	});


	$(".tituloMenu").mouseout(function(event){
		var varAlt = $(this).attr('alt');
		if(varAlt == 'Inicial'){
			$(".tituloMenu").removeClass('actionMenu');
			$("#containerInical").addClass('conteudoActiveMenu');
		}
		event.preventDefault();
	});

	return false;

});