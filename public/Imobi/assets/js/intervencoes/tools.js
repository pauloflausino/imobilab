(function($){

    $.fn.smartTools = function(customOptions){

        url = location.href;

        function getArrayUrlBaseIndex() {

            if (url.search("index.php") > 0) {
                baseURL = url.substring(0, url.indexOf('index.php', 14)) + "index.php";
                moreLength = 1;
            } else {
                baseURL = $("#sa-baseurl").val();
                moreLength = 0;
            }

            baseURLIndex = baseURL;

            var arrayDtReturn = new Array();

            arrayDtReturn[0] = baseURLIndex;
            arrayDtReturn[1] = moreLength;

            return arrayDtReturn;

        }

        function getToolAppName() {
            appName = $("#sa-appname").val();
            return appName;
        }

        function getToolControllerName() {
            appController = $("#sa-appcontroller").val(); // url.substring($("#sa-baseurl").val().length, url.length);
            return appController;
        }

        function getToolSchema() {
            appSchema = $("#sa-appschema").val();
            return appSchema;
        }

        function getToolParams() {
            arrayURLIndex = getArrayUrlBaseIndex();

            params = url.substring(Number(arrayURLIndex[0].length + getToolControllerName().length + 1 + getToolAppName().length + arrayURLIndex[1]), url.length);

            return params;
        }

        function getToolAction() {

            action = getToolAppName();
            if (action.indexOf("/") > 0) {
                action = action.substring(0, action.indexOf("/"));
            }

            return action;

        }
        /*
        @todo - Criar exibição de alertas por aplicativo
        function showAlertsApplication() {

            $(layerOptions).find(".callAlertsApplication").unbind("click").click(function(){

                wUrl = _sa_baseUrl() + "tools/options/menu";

            });

        }*/

        function showStatsAccount() {
            b = new Boxy("<div>Aguarde ... carregando ...</div>", {modal: true, title: "&nbsp;", unloadOnHide: true});
            wUrlStats = _sa_baseUrl() + "account/resources/status";
            $.get(wUrlStats, function(callback){
                b.setContent(callback);
                b.getContent().find(".tabs ul").idTabs("geral");
                b.center();
            });
        }

        function getToolMenu(layerOptions, layerBase, pointerOffSetLeft, b) {

            wUrlToolMenu = _sa_baseUrl() + "tools/options/menu";
            $.post(wUrlToolMenu, {app: getToolAppName(), schema: getToolSchema(), params: getToolParams(), action: getToolAction()}, function(callback){

                $(layerOptions).html(callback);

                b.center();

                addNavigationPoint(layerBase, b);
                removeNavigationPoint(layerBase, b);
                _sa_addNewUser();

                if ($("#opt-helpme").attr("href") != null) {
                    $(layerOptions).find(".callHelpMe").click(function(){
                        $("#opt-helpme").trigger("click");
                    });
                } else {
                    $(layerOptions).find(".callHelpMe").parent().remove();
                }

                loadWindowUsuariosPermissions();
                //showAlertsApplication();

                $(layerOptions).find(".statsAccount").click(function(){
                    showStatsAccount();
                });

            });

        }

        defaultOptions = {
            width : "24",
            height : "19",
            medida : "px",
            pathImage : $("#sa-baseurl").val() + "/libs/imgs/tool-options.png",
            refItem : "layerPointer",
            ref : ".subinfo",
            value : "0",
            window : ".container_12",
            urlPostAdd : $("#sa-baseurl").val() + "preferencias/favoritos/adicionar",
            urlPostRemove : $("#sa-baseurl").val() + "preferencias/favoritos/remover",
            action: getToolAction(),
            params: getToolParams()
        }
        opts = $.extend(defaultOptions,customOptions);

        // Função responsável por criar o ponto de ação no topo do aplicativo.
        function pointCreateActionArea() {

            oWRef = jQuery(opts.window);

            if (opts.action.length > 0) {

                divLayer = document.createElement('div');

                $(divLayer).css({
                    "width" : "780px"
                });

                //areaAction = document.createElement('div');
                areaAction = $(".option-atalhos-rapidos");
                colorIcon = $(".icoInfo").find("h2").css("color");

                objHeader = $(".sm-header-app").html();

                if (objHeader != undefined) {
                    /*
                    jQuery(areaAction).attr({
                        "id":opts.refItem,
                        "title" : "Atalhos rápidos"
                        }).css({
                        "width":opts.width + opts.medida,
                        "height":opts.height + opts.medida,
                        "position":"absolute",
                        "margin-left" : ($(".sm-header-app").css("width").replace("px","")-40),
                        "margin-top": "-12" + opts.medida,
                        "cursor":"pointer",
                        "text-align" : "center",
                        "color" : colorIcon
                    }).html("<span class='sm-font-15em' style='color:" + colorIcon + ";'><i class='icon-info-sign'></i></span>");
                    jQuery(areaAction).tipsy({gravity:"s", html: true});

                    $(divLayer).append(areaAction);

                    jQuery(opts.window).find(opts.ref).append(divLayer);
                    */
                }

                $(areaAction).click(function(){
                    objThis = $(this);
                    objThis.css({"opacity":"1.0"});
                    pointLoadOptions(divLayer, objThis);
                });

                $(areaAction).show();

            }

        }

        // função responsável por carregar as opções referentes
        function pointLoadOptions(layer, objClick) {

            $("body").unbind("click");

            //pointerOffSetLeft = $(layer).find("#layerPointer").css("margin-left").replace("px", "");

            boxie = new Boxy("<div>Aguarde ... carregando informações...</div>", {title: "&nbsp;", modal: true});
            optionsLayer = boxie.getContent();

            getToolMenu(optionsLayer, layer, false, boxie);

        }

        function removeNavigationPoint(layer, bo) {

            $(".removeNavigationPoint").click(function(){

                wUrlNavigationRemove = opts.urlPostRemove;

                objVars = {
                    action : opts.action,
                    schema : getToolSchema(),
                    params : opts.params,
                    value : $(this).attr("alt")
                }
                new Boxy.confirm("Tem certeza que deseja <b>apagar</b> o ponto de navegação ?", function(callback){

                    bo.hide();

                    $.post(wUrlNavigationRemove, objVars, function(data){

                        $.jGrowl("Ponto de navegação foi apagado.",{
                            header:"Atenção"
                        });

                    }, "json");

                },{title: "Atenção"});

            });
        }

        function addNavigationPoint(layer, bo) {

            $(".addNavigationPoint").click(function(){

                wUrlNavigationAdd = opts.urlPostAdd;

                objVars = {
                    action : opts.action,
                    schema : getToolSchema(),
                    params : opts.params,
                    value : $(this).attr("alt")
                }

                $.post(wUrlNavigationAdd, objVars, function(data){

                    $.jGrowl("Favorito salvo com sucesso",{
                        header:"Atenção"
                    });

                    bo.hide();

                    b = new Boxy(data.conteudo,{
                        title:"Adicionar titulo",
                        modal: true
                    });

                    //$("#" + opts.refItem).trigger("click");

                    switch(data.action) {
                        case "registrar":

                            $("form[name='formAddFavorito']").find("input[type='hidden']").each(function(){
                                switch($(this).attr("name")) {
                                    case "params":
                                        $(this).val(opts.params);
                                        break;
                                    case "action":
                                        $(this).val(opts.action);
                                        break;
                                }
                            });
                            objAppFavoriteTitle = $(".icoInfo").find("h2").text();
                            $("form[name='formAddFavorito']").find("input[name='titulo']").focus();
                            $("form[name='formAddFavorito']").submit(function(){
                                dataSerializeArray = jQuery(this).serialize() + "&value=" + objVars.value;
                                $("form[name='formAddFavorito']").find("input[type='submit']").attr({"disabled":"disabled"}).val("Salvando favorito...");
                                $.post(opts.urlPostAdd,dataSerializeArray,function(data){
                                    b.hide();
                                    $.jGrowl("Detalhes do favorito foram salvos com sucesso",{
                                        header:"Atenção"
                                    });
                                });
                                return false;
                            });
                            break;
                    }

                    jQuery(divLayer).attr({
                        "title":"Remover favorito"
                    });

                }, "json");

            });
        }

        pointCreateActionArea();

    }

})(jQuery);