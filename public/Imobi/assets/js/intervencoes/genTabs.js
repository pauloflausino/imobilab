(function($){
    $.fn.genTabs = function(customAttrs, callback) {
	
	defaultOpts = {
	    typeNode : "li",
	    tabOnLoad : ".selected",
	    showIn : "#sm-window-content-usuarios",
	    window : "#sm-window-usuarios"
	}
	
	genTabsOpts = $.extend(true, defaultOpts, customAttrs);
	
	objThis = this;

        objNode = "";
        if (genTabsOpts.boxy != undefined) {
            areaWindow = $(genTabsOpts.boxy.getContent()).find(genTabsOpts.typeNode);
        } else {
            areaWindow = $(objThis).find(genTabsOpts.typeNode);
        }

        function menu_bar() {
            // check se a barra já foi criada.
            objBar = $(genTabsOpts.showIn).parent().find("#menu-bar");
            if (objBar.length == 0) {
                $(genTabsOpts.showIn).before("<div id='menu-bar'></div>");
            }
            
            return $(genTabsOpts.showIn).parent().find("div#menu-bar");
        }

        function remove_bar() {
            $(genTabsOpts.showIn).parent().find("#menu-bar").remove();
        }

        function genTabsLoadingCenter(a) {

            img = document.createElement("img");
            attrsImg = {
                "src": _sa_baseUrl() + "libs/imgs/loading-window.gif"
            }
            $(img).attr(attrsImg).css({
                "padding":"10px"
            });

            p = document.createElement("p");
            $(p).text("Carregando informações...");

            div = document.createElement("div");
            $(div).append(img).append(p);
            $(div).css({
               "text-align":"center",
               "padding":"50px"
            });

            $(a).html(div);
            //_sa_favoritosSetDataCenter(div);

        }

        function menu_create(objBar, node) {
            i = 0;
            $(objBar).html("");
            $(node).each(function(index,element){
                a = "<a href='javascript:void(0)' class='" + element[index].classe +"'>" + element[index].texto + "</a>";
                if (i > 0) {
                    a += " | ";
                }
                objBar.append(a);
                i++;
            });
        }
	
	areaWindow.each(function(){
	    
	    $(this).find("a").click(function(){
                objA = this;
                if (genTabsOpts.menu.create) {

                    objNodeOptions = $(this).attr("alt").split(":");
                    node = eval("genTabsOpts.menu.nodes." + objNodeOptions[1]);
                    if (node != undefined) {
                        wBar = menu_bar();
                        menu_create(wBar, node);
                    } else {
                        remove_bar();
                    }

                }
                
                $(this).parent().parent().find("li").removeClass();
                $(this).parent().addClass("selected");
                
                wUrlLoad = $(this).attr("href");
                objWindowIn = $(genTabsOpts.window).find(genTabsOpts.showIn);
                genTabsLoadingCenter(objWindowIn);
                
		$.post(wUrlLoad, {}, function(data){
                    //$(genTabsOpts.showIn).before("<div><p><a href='javascript:void(0)'>Teste</a></p></div>");
		    $(genTabsOpts.window).find(genTabsOpts.showIn).html(data);
		    if (typeof callback == "function") {
			callback.call(data);
		    }

		});
                
		return false;

	    });
	    
	    // verifica se tem alguma tab definida para carregar no in�cio.
	    getClassName = "." + $(this).attr("class");
	    if (getClassName == genTabsOpts.tabOnLoad) {
		$(this).find("a").trigger("click");
	    }
	    
	});

        return false;
    
    }
})(jQuery);