function loadingPrefsMsg() {
    return "<div class='paddingBox'><br><div class='tlfLoading' style='margin: 0 auto;'></div><br><p align='center'>Processando informações</p><br></div>";
}

function optSearchIntervencao() {

    $(".opt-search-intervencao").unbind("click").click(function(){

        $(this).text("Aguarde ...").unbind("click");

        $("form[name='intervencoes_search']").trigger("submit");

        $(this).html('<i class="icon-search"></i>');
        return false;

    });

}

function processSearchDashIntervencoes() {

    objDashIntervencoes = $("#epmts-dash-intervencoes");

    loadSearchProperties(objDashIntervencoes, true);

    objFI = $("form[name='intervencoes_search']");

    objFI.find("input").unbind("keypress").keypress(function(e){
        if (e.which == 13) {
            objFI.trigger("submit");
        }
    });

    objFieldRegional = objFI.find("#selectRegional");
    objFieldRegional.unbind("change").change(function(){

        objValue = $(this).val();

        if (objValue.match(/\S/) && objValue != "null") {

            wUrlUF = baseUrlApp() + "/epmts/fp/uf/getAllByRegional";
            objForm.find("select[name='s[i___unid_fed]']").html("<option value=''>CARREGANDO...</option>");
            $.post(wUrlUF,{regional: objValue}, function(callback){
                objForm.find("select[name='s[i___unid_fed]']").html(callback);
            });

        }

    });

    objFI.unbind("submit").submit(function(){

        wUrlIntervencoesSearch = baseUrlApp() + "/epmts/buscar/intervencoes/resultados";
        objSerialize = $(this).serialize();

        $.post(wUrlIntervencoesSearch, objSerialize, function(callback){

            optSearchIntervencao();

            window.location.hash = "";

            objDashIntervencoes.find("#resultados").html(callback);
            $("a[href='#resultados']").trigger("click");

            formatTipsy();

            processResultsSearch(objDashIntervencoes.find("#resultados"), objDashIntervencoes);

        });

        return false;

    });

    $(".toggle-fields").unbind("click").click(function(){
        objAlt = $(this).attr("alt");
        objField = $(".fields-opt-" + objAlt);
        isVisible = objField.is(":visible");
        if (isVisible) {
            objField.hide();
        } else {
            objField.show();
        }
    });

    optSearchIntervencao();
    /*
    $('.btnAcoesOcultarTitulo').click(function(event){
        $(this).parent('h4').next('div').hide();
        $(this).hide();
        $(this).parent('h4').find('.btnAcoesExibirTitulo').show();
        event.preventDefault();
    });

    $('.btnAcoesExibirTitulo').click(function(event){
        $(this).parent('h4').next('div').show();
        $(this).hide();
        $(this).parent('h4').find('.btnAcoesOcultarTitulo').show();
        event.preventDefault();
    });*/

}

function processResultsSearch(objResultados, objDashIntervencoes) {

    objResultados.find(".open-intervencao").unbind("click").click(function(){

        idIntervencao = $(this).attr("alt");
        openDetailsIntervencao(idIntervencao, objDashIntervencoes);

    });

}

function historicoOptions(idIntervencao, idAtividade, objClick, b) {

    $(".hist-detalhes-view").unbind("click").click(function(){

        id = $(this).attr("alt");

        bo = new Boxy(loadingPrefsMsg(),{title:"",modal:true,unloadOnHide:true});

        wUrlDetalhesView = baseUrlApp() + "/epmts/intervencoes/atividades/historico/detalhes/" + id;
        $.get(wUrlDetalhesView, function(callback){

            bo.setContent(callback);
            bo.center();

        });

    });

}

function closeWindowBoxy(boxy, obj)
{
    Boxy.get(obj).hide();
    $('.paddingBox').hide();
    $('.boxy-modal-blackout').hide();
}

function openDetailsIntervencao(idIntervencao, objDashIntervencoes, position) {

    $("a[href='#detalhes']").trigger("click");
    objDashIntervencoes.find("#detalhes").html(loadingPrefsMsg());


    wUrlOpenIntervencaoDetalhes = baseUrlApp() + "/epmts/buscar/intervencoes/detalhes/" + idIntervencao;

    $.get(wUrlOpenIntervencaoDetalhes, function(callback){

        window.location.hash = "#detalhes," + idIntervencao;

        if (objDashIntervencoes != undefined) {

            //$(".btnAcoesOcultarTituloA").trigger("click");

            objDashIntervencoes.find("#detalhes").html(callback);

            objA = $(".atividade-parent-a").attr("class");
            if (objA != undefined) {
                optionsIntervencoes($(".atividade-parent-a"), 'a', idIntervencao, objDashIntervencoes, false, position);
            }

            objB = $(".atividade-parent-b").attr("class");

            //optionsIntervencoes($(".atividade-parent-b"), 'b', idIntervencao, objDashIntervencoes);

            if (objB != undefined && objA != undefined) {
                optionsIntervencoes($(".atividade-parent-b"), 'b', idIntervencao, objDashIntervencoes, false, position);
            } else {
                optionsIntervencoes($(".atividade-parent-b"), 'a', idIntervencao, objDashIntervencoes, false, position);
            }

            $(".campo-data-intervencao").mask("99/99/9999");

            $(".campo-data-intervencao").each(function(index, element){
                objDisabled = $(element).attr("readonly");
                if (objDisabled == "readonly") {
                }
            });

            $(".campo-data-intervencao").datepicker({
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                nextText: 'Próximo',
                prevText: 'Anterior'
            });
            /*
            $(".tlf-toggle-etapa").unbind("click").click(function(){
                objDisplays = $(this).find("span");
                objL = objDisplays.length;
                for(i=0; i < objL; i++) {
                    objBase = $(this).find("span:eq(" + i + ")");
                    objDisplay = objBase.css("display");
                    if (objDisplay == "block") {
                        console.log(objBase);
                        //$(this).find("span:eq(" + i + ")").trigger("click");
                    }
                }
            });*/
            loadSearchProperties(objDashIntervencoes.find("#detalhes"), false);
            //console.log(objDashIntervencoes.find("#detalhes").find(".btnToggleDataSearch"));
            //objDashIntervencoes.find("#detalhes").find(".btnToggleDataSearch").trigger("click");

            // move to position
            if (position != undefined) {
                $('.scroller').scrollTop(position.top);
            }

        }

    });

}

function triggerAnexarArquivo(intervencao, atividade, b, idIntervencao, objDashIntervencoes, positionWindow) {

    $("#multiple-" + intervencao + "-" + atividade).unbind();
    $("#multiple-" + intervencao + "-" + atividade).html5Uploader({
        name: "Filedata",
        postUrl: $("#url_app_upload").val(),
        onSuccess: function callbackSuccess(e, file, response, fieldHandler) {

            responseJSON = response;
            var obj = jQuery.parseJSON(responseJSON);

            objIdIntervAtiv = fieldHandler.parent().find("input[name='id_interv_ativ']").val().split("/");

            // salva o arquivo no lancamento
            salvaInformacoesArquivo(obj, objIdIntervAtiv[0], objIdIntervAtiv[1], b, idIntervencao, objDashIntervencoes, positionWindow);

        },
        onClientLoadStart: function callbackLoadStart(e, file) {

        }

    });

}

function salvaInformacoesArquivo(params, intervencao, atividade, b, idIntervencao, objDashIntervencoes, positionWindow) {

    wUrlSalvarInformacoesArquivo = baseUrlApp() + "/epmts/intervencoes/atividades/arquivos/anexar/" + intervencao + "/" + atividade;
    tlfShowLockWindow();
    objParams = params;
    $.post(wUrlSalvarInformacoesArquivo, objParams, function(callback){

        tlfHideLockWindow();

        if (callback.type == "sucess") {
            // recarrega os arquivos para o lancamento
            openDetailsIntervencao(idIntervencao, objDashIntervencoes, positionWindow);
            b.hide();

            recarregarArquivosAnexados(intervencao, atividade, idIntervencao, objDashIntervencoes, function(){
                alertLockWindow("Arquivo carregado com sucesso");
            });

        }

    }, "json");

}

function recarregarArquivosAnexados(intervencao, atividade, idIntervencao, objDashIntervencoes, callbackf, positionWindow) {

    wUrlArquivosAnexados = baseUrlApp() + "/epmts/intervencoes/atividades/arquivos/anexados/" + intervencao + "/" + atividade;

    bo = new Boxy(loadingPrefsMsg(),{title: "", modal: true, unloadOnHide: true});

    $.get(wUrlArquivosAnexados, function(callback){

        bo.setContent(callback);

        formatGrids();

        optionsArquivosAnexos(bo, idIntervencao, objDashIntervencoes, positionWindow);

        bo.center();

        if (typeof callbackf === "function") callbackf();

    });

}

function recarregarObservacoes(id_observacao, objArea, node, idIntervencao, objDashIntervencoes, callbackf, positionWindow) {

    box = new Boxy(loadingPrefsMsg(),{modal: true, unloadOnHide: true, title: ""});

    wUrlOpenObs = baseUrlApp() + "/epmts/intervencoes/atividades/observacoes/visualizar/" + id_observacao;
    $.get(wUrlOpenObs, function(callback){

        box.setContent(callback);
        box.center();

        optionsIntervencoes(objArea, node, idIntervencao, objDashIntervencoes, box, positionWindow);

        if (typeof callbackf === "function") callbackf();
    });

}

function recarregarInterdependencia(id, objArea, node, idIntervencao, objDashIntervencoes, callbackf, positionWindow) {

    wUrlInterdepView = baseUrlApp() + "/epmts/intervencoes/atividades/interdep/" + idIntervencao + "/" + id;

    b = new Boxy(loadingPrefsMsg(),{title:"", modal:true, unloadOnHide: true});

    $.get(wUrlInterdepView, function(callback){

        b.setContent(callback);

        formatGrids();
        b.center();

        idIntervBase = $("#detalhes").find("input[name='id_intervencao']").val();

        optionsIntervencoes(objArea, node, idIntervBase, objDashIntervencoes, b, positionWindow);

        if (typeof callbackf === "function") callbackf();

    });

}

function recarregarInterdependenciaInterv(id, objArea, node, idIntervencao, objDashIntervencoes, positionWindow) {

    wUrlInterdepView = baseUrlApp() + "/epmts/intervencoes/interdep/" + idIntervencao;

    b = new Boxy(loadingPrefsMsg(),{title:"", modal:true, unloadOnHide: true});

    $.get(wUrlInterdepView, function(callback){

        b.setContent(callback);

        formatGrids();
        b.center();

        objContent = b.getContent();

        objContent.find(".toggle-interdep-ponta").click(function(){
            objAlt = $(this).attr("alt");
            objContent.find("div[class^='ativ-interdep-']").hide();
            objContent.find("div[class='ativ-interdep-" + objAlt + "']").show();
        });

        objContent.find(".toggle-interdep-ponta:eq(0)").trigger("click");

    });

}

function optionsArquivosAnexos(b, idIntervencao, objDashIntervencoes, positionWindow) {

    $(".trigger-anexar-arquivo").unbind("click").click(function(){
        $(this).hide();
        objAlt = $(this).attr("alt");
        objAltSplit = $(this).attr("alt").split("-");
        $("#multiple-" + objAlt).show();
        triggerAnexarArquivo(objAltSplit[0], objAltSplit[1], b, idIntervencao, objDashIntervencoes, positionWindow);
    });
/*
    $(".apagar-anexo").unbind("click").click(function(){
        objAlt = $(this).attr("alt").split(",");
        Boxy.confirm("TEM CERTEZA QUE DESEJA APAGAR O ANEXO ?", function(){

            wUrlApagarAnexo = baseUrlApp() + "/mooney/lancamentos/arquivos/remover/" + objAlt[1];
            tlfShowLockWindow();
            $.post(wUrlApagarAnexo, {lancamento: objAlt}, function(callback){
                tlfHideLockWindow();
                alertLockWindow(callback.msg);
                recarregarArquivosAnexados(objAlt[0], idIntervencao, objDashIntervencoes);
            }, "json");

        },{title:"Atenção"});
    });*/

}

function activeGroupsVisible() {
    i=0;
    $("input[name^='setGroupView']").each(function(ind4, ele4){

        if ($(ele4).attr("checked") == "checked") {
            changeViewItem($(ele4).val());
            i++;
        }
    });
    if (i==0) {
        $(".show-intervs-noativ").show();
    } else {
        $(".show-intervs-noativ").hide();
    }
}

function checkGroupsVisible() {
    i=0;
    $("div[class^='ativ-show-']").each(function(ind3, ele3){
        objDisplay = $(ele3).css("display");
        objIdGroup = $(ele3).find("input[name='grp_id']").val();

        if (objDisplay != "none") {
            $("input[name^='setGroupView']").each(function(ind4, ele4){
                if ($(ele4).val() == objIdGroup) {
                    i++;
                    $(ele4).attr({"checked":"checked"});
                }
            });
        } else {
            $(ele3).removeAttr("checked");
        }
    });
    if (i==0) {
        $(".show-intervs-noativ").show();
    } else {
        $(".show-intervs-noativ").hide();
    }
}

function changeViewItem(id) {
    $(".ativ-show-" + id).each(function(ind2, ele2){
        objDisplay = $(ele2).css("display");

        if (objDisplay == "none") {
            $(ele2).parent().show();
            $(ele2).show();
        } else {
            $(ele2).parent().hide();
            $(ele2).hide();
            $("input[name^='setGroupView']").each(function(ind4, ele4){
                if ($(ele4).val() == id) {
                    $(ele4).removeAttr("checked");
                }
            });

        }
    });
}

function editIntervCallback(t) {
    $(".close-area-mdetalhes").trigger("click");
    openDetailsIntervencao($("input[name='id_intervencao']").val(), $("#epmts-dash-intervencoes"));
}

function getTotalAtividadesExibicao(node, idPosition) {

    totalDisponivel = 0;
    totalVisivel = 0;
    itemActive = 0;

    $("div[class*='atividade-parent-" + node + "']").each(function(index, element){

        vDisplay = $(element).css("display");

        if (vDisplay == "block") {
            totalVisivel++;
            idPosHa = $(element).find('form').find("input[name='id_posicao']").val();

            if (idPosHa == idPosition) {
                itemActive = totalVisivel;
            }
        }

        totalDisponivel++;

    });

    vars = {totalDisponivel: totalDisponivel, totalVisivel: totalVisivel, itemActive: (itemActive-1)};

    return vars;

}

function getPositionWindow(node, idPosHa) {

    getTotalActive = getTotalAtividadesExibicao(node, idPosHa);

    positionWindow = {};
    positionWindow.top = Number(280+(200*getTotalActive.itemActive)+heightSite);

    return positionWindow;

}

function optionsIntervencoes(objArea, node, idIntervencao, objDashIntervencoes, b, positionWindow) {

    var varScrollPositionOld = $('.scroller').scrollTop();

    $('.scroller').scroll(function(){
        var varTop = $(this).scrollTop();
        if(varScrollPositionOld > varTop && varTop > 300){
            $('.menuFlutuante').show();
        }else{
            $('.menuFlutuante').hide();
        }
        varScrollPositionOld = varTop;
    });
    /*
    $('.btnAcoesOcultarTituloA').click(function(event){
        $(this).parent('h4').next('div').hide();
        $(this).hide();
        $(this).parent('h4').find('.btnAcoesExibirTituloA').show();
        event.preventDefault();
    });

    $('.btnAcoesExibirTituloA').click(function(event){
        $(this).parent('h4').next('div').show();
        $(this).hide();
        $(this).parent('h4').find('.btnAcoesOcultarTituloA').show();
        event.preventDefault();
    });

    $('.btnAcoesOcultarTitulo').click(function(event){
        $(this).parent('h4').next('div').hide();
        $(this).hide();
        $(this).parent('h4').find('.btnAcoesExibirTitulo').show();
        event.preventDefault();
    });

    $('.btnAcoesExibirTitulo').click(function(event){
        $(this).parent('h4').next('div').show();
        $(this).hide();
        $(this).parent('h4').find('.btnAcoesOcultarTitulo').show();
        event.preventDefault();
    });*/

    $('.btnAcoesTravarTitulo').click(function(event){
        $(this).hide();
        $(this).parent('h4').find('.btnAcoesDestravarTitulo').show();
        $('.colunaB').removeClass('destravarColuna');
        event.preventDefault();
    });

    $('.btnAcoesDestravarTitulo').click(function(event){
        $(this).hide();
        $(this).parent('h4').find('.btnAcoesTravarTitulo').show();
        $('.colunaB').addClass('destravarColuna');
        event.preventDefault();
    });

    $('.btnAcoesTravarTituloA').click(function(event){
        $(this).hide();
        $(this).parent('h4').find('.btnAcoesDestravarTituloA').show();
        $('.colunaA').removeClass('destravarColuna');
        event.preventDefault();
    });

    $('.btnAcoesDestravarTituloA').click(function(event){
        $(this).hide();
        $(this).parent('h4').find('.btnAcoesTravarTituloA').show();
        $('.colunaA').addClass('destravarColuna');
        event.preventDefault();
    });

    $(".tipsy").hide();

    tlfLoadLockWindow();

    // hide all activities
    $("div[class*='ativ-show-']").each(function(index, element){

        grp_id = $(element).find("input[name='grp_id']").val();

        inpt = $("input[value='grp-" + grp_id + "']").attr("checked");

        if (inpt == "checked") {

            $(element).parent().show();
            $(element).show();
        } else {
            $(element).hide();
            $(element).parent().hide();
        }

    });

    idSecaoResponsavel = $("#secao_responsavel_field").val();
    $("div[class*='ativ-show-" + idSecaoResponsavel + "']").parent().show();
    $("div[class*='ativ-show-" + idSecaoResponsavel + "']").show();

    $(".toggle-etapa-atividades").unbind("click").click(function(){
        objThis = $(this);
        objAlt = $(this).attr("alt");
        isVisible = $("div[class*='atividades-etapa-" + objAlt + "']").parent().is(":visible");
        if (isVisible) {
            $("div[class*='atividades-etapa-" + objAlt + "']").parent().hide();
            $("div[class*='atividades-etapa-" + objAlt + "']").hide();
            objThis.find("i").removeClass("icon-angle-down").addClass("icon-angle-right");
        } else {
            $("div[class*='atividades-etapa-" + objAlt + "']").parent().show();
            $("div[class*='atividades-etapa-" + objAlt + "']").show();
            objThis.find("i").removeClass("icon-angle-rigth").addClass("icon-angle-down");
        }
    });

   // $( ".date-input" ).datepicker({ dateFormat: 'yy-dd-mm' });

    $(objArea).find(".change-date-atividade").unbind("keypress").keypress(function(e){

        objParent = $(this).closest("div[class*='atividade-parent-'");

        objThis = $(this);
        fieldName = $(this).attr("name");
        idIntervencaoBase = $("input[name='id_intervencao']").val();
        idPosicao = $("input[name='id_posicao']").val();

        idAtividade = objParent.find("input[name='id_atividade']").val();
        idDivisao = objParent.find("input[name='divisao']").val();
        id_intervencao = objParent.find("input[name='id_intervencao_']").val();
        //id_intervencao = objParent.find("input[name='id_intervencao_']").val();

        if (e.which == 13) {

            objThis.css({"color":"black"});

            tlfShowLockWindow();

            if ($(this).attr('name') == 'rp[inicio]' || $(this).attr('name') == 'rp[termino]') {

                box = new Boxy(loadingPrefsMsg(), {title: "", modal: true, unloadOnHide: true});

                wUrlMotivoReplan = baseUrlApp() + "/epmts/intervencoes/motivo/replan/" + idDivisao;
                $.get(wUrlMotivoReplan,function(htmlReplan){

                    tlfHideLockWindow();

                    box.setContent(htmlReplan);

                    box.center();
                    var objContent = box.getContent();

                    objContent.find('#motivo_replan').focus();

                    //executarReplan(wUrlRegistrarData, objSerialize, objThis, candidato);
                    $('.btn_mtv_replan').on('click', function () {

                        replanid = objContent.find('#replan_id').val();
                        motivoReplan = objContent.find("#motivo_replan").val();

                        if (!replanid.match(/\S/)) {
                            alertLockWindow("Você precisa selecionar o Motivo Replan.");
                            return false;
                        }

                        if (!motivoReplan.match(/\S/)) {
                            alertLockWindow("Você precisa descrever o Motivo Replan.");
                            return false;
                        }

                        wUrlRegistrarData = baseUrlApp() + "/epmts/intervencoes/registrar/data/" + id_intervencao + "/" + idAtividade + "/" + node;
                        objSerialize = $(objThis).closest('form').serialize() + "&motivo_replan=" + $('#motivo_replan').val() + "&replanid=" + replanid + "&field=" + fieldName;

                        //positionWindow = $(objThis).closest('td').offset();
                        heightSite = $(".site-cronograma-" + node).height();
                        idPosHa = $(objThis).closest('form').find("input[name='id_posicao']").val();

                        // verificar a quantidade de atividades em exibição
                        // getTotalActive = getTotalAtividadesExibicao(node, idPosHa);

                        // positionWindow = {};
                        // positionWindow.top = Number(280+(200*getTotalActive.itemActive)+heightSite);
                        positionWindow = getPositionWindow(node, idPosHa);

                        box.hide();

                        tlfShowLockWindow();

                        $.post(wUrlRegistrarData, objSerialize, function(callback){

                            tlfHideLockWindow();

                            if (callback.type == "sucess") {
                                openDetailsIntervencao(idIntervencaoBase, objDashIntervencoes, positionWindow);
                            } else {
                                objThis.css({"color":"red"});
                                objThis.val('');
                                objThis.css({"color":"black"});
                            }

                            alertLockWindow(callback.msg);

                        },"json");
                    });

                });

            } else {

                wUrlRegistrarData = baseUrlApp() + "/epmts/intervencoes/registrar/data/" + id_intervencao + "/" + idAtividade + "/" + node;
                objSerialize = $(objThis).closest('form').serialize() + "&field=" + fieldName;

                //positionWindow = $(objThis).closest('td').offset();
                heightSite = $(".site-cronograma-" + node).height();
                idPosHa = $(this).closest('form').find("input[name='id_posicao']").val();

                // verificar a quantidade de atividades em exibição
                // getTotalActive = getTotalAtividadesExibicao(node, idPosHa);

                // positionWindow = {};
                // positionWindow.top = Number(572+(200*getTotalActive.itemActive)+heightSite);
                positionWindow = getPositionWindow(node, idPosHa);

                $.post(wUrlRegistrarData, objSerialize, function(callback){
                    tlfHideLockWindow();
                    if (callback.type == "sucess") {
                        openDetailsIntervencao(idIntervencaoBase, objDashIntervencoes, positionWindow);
                    } else {
                        objThis.css({"color":"red"});
                        objThis.val('');
                        objThis.css({"color":"black"});
                    }

                    alertLockWindow(callback.msg);

                },"json");
            }

        }

    });

    $(objArea).find(".set-date-actual-inicio").unbind("click").click(function(){
        $('.tipsy').hide();
        $(this).closest("form").find("input[name='ac[inicio]']").val($("#date_base_actual").val());
        $(this).closest("form").find("input[name='rp[inicio]']").val($("#date_base_actual").val());
        var e = jQuery.Event("keypress");
        e.which = 13;
        $(this).closest("form").find("input[name='rp[inicio]']").trigger(e);
    });

    $(objArea).find(".set-date-actual-termino").unbind("click").click(function(){
        $('.tipsy').hide();
        $(this).closest("form").find("input[name='ac[termino]']").val($("#date_base_actual").val());
        $(this).closest("form").find("input[name='rp[termino]']").val($("#date_base_actual").val());
        var e = jQuery.Event("keypress");
        e.which = 13;
        $(this).closest("form").find("input[name='rp[termino]']").trigger(e);
    });

    $(".trigger-check-anexos").unbind("click").click(function(){

        $('.tipsy').hide();

        objAltSplit = $(this).attr("alt").split("/");

        heightSite = $(".site-cronograma-" + node).height();
        idPosHa = $(this).closest('form').find("input[name='id_posicao']").val();

        positionWindow = getPositionWindow(node, idPosHa);

        recarregarArquivosAnexados(objAltSplit[0], objAltSplit[1], idIntervencao, objDashIntervencoes, false, positionWindow);

    });

    $(".open-details-intervencao").unbind("click").click(function(){

        $(".tipsy").hide();

        idIntervencao = $(this).attr("alt");

        // open details
        b = new Boxy(loadingPrefsMsg(),{title:"",closeable: false,modal: true, unloadOnHide: true});

        wUrlIntervencaoDetails =  baseUrlApp() + "/epmts/buscar/intervencoes/mdetalhes/" + idIntervencao;
        $.get(wUrlIntervencaoDetails, function(callback){

            b.setContent(callback);
            b.center();

        });

    });

    $(".open-details-site").unbind("click").click(function(){

        $(".tipsy").hide();

        idIntervencao = $(this).attr("alt");

        // open details
        b = new Boxy(loadingPrefsMsg(),{title:"", modal: true, unloadOnHide: true});

        wUrlIntervencaoDetails =  baseUrlApp() + "/epmts/buscar/intervencoes/sites/detalhes/" + idIntervencao;
        $.get(wUrlIntervencaoDetails, function(callback){
            callback = callback + '<input type="hidden" name="id_intervencao_site" id="id_intervencao_site" value="'+idIntervencao+'">';
            b.setContent(callback);
            b.center();

            optionsIntervencoes(objArea, node, idIntervencao, objDashIntervencoes, b);

        });

    });

    $(".interv-site-editar").unbind("click").click(function(){

        idSite = $(this).attr("alt");

        if (idSite === undefined) {
            idSite = $('#id_intervencao_site').val();
        }

        wUrlIntervSite = baseUrlApp() + "/epmts/sites/edit/" + idSite;
        //tlfShowLockWindow();
        $(this).genWindowCrudCreate("form[name^='sites_']",{
            title: "",
            modal: true
        }, wUrlIntervSite, function(callbackRequest){

            //tlfHideLockWindow();

            objCon = callbackRequest.window.getContent();
            objCon.find("input[type='text']:eq(0)").focus();

            formSites(callbackRequest);

        }, function(callbackPost){

            //tlfHideLockWindow();

            if (callbackPost.req.status == "sucess") {
                alertLockWindow("O site foi atualizado com sucesso");
                callbackPost.window.hide();
                b.hide();
                $(".close-area-detalhes-site").trigger("click");
            } else {
                alertLockWindow("Atenção! corrija os erros informados e tente novamente");
            }

        });

        return false;

    });

    $(".interv-interdep-view").unbind("click").click(function(){

        $('.tipsy').hide();

        objAlt = $(this).attr("alt");

        recarregarInterdependenciaInterv(objAlt, objArea, node, idIntervencao, objDashIntervencoes);

    });

    $(".interdep-view").unbind("click").click(function(){

        $('.tipsy').hide();

        objThis = this;
        heightSite = $(".site-cronograma-" + node).height();
        idPosHa = $(objThis).closest('form').find("input[name='id_posicao']").val();

        positionWindow = getPositionWindow(node, idPosHa);

        objAlt = $(this).attr("alt").split("/");

        recarregarInterdependencia(objAlt[1], objArea, node, objAlt[0], objDashIntervencoes, false, positionWindow);

    });

    $(".interv-diretriz").unbind("click").click(function(){

        idIntervencao = $(this).attr("alt");
        wUrlAtivIntervDiretriz = baseUrlApp() + "/epmts/intervencoes/diretriz/" + idIntervencao;

        b = new Boxy(loadingPrefsMsg(), {title: "", modal: true, unloadOnHide: true});
        b.center();

        $.get(wUrlAtivIntervDiretriz, function(callback){

            b.setContent(callback);
            formatGrids();
            b.center();

            objContent = b.getContent();

            // toggle pontas
            $(".toggle-diretriz-ponta").unbind("click").click(function(){
                objAlt = $(this).attr("alt");
                if (objAlt == 'b') {
                    objContent.find(".tg-ponta-a").hide();
                    objContent.find(".tg-ponta-b").show();
                } else {
                    objContent.find(".tg-ponta-a").show();
                    objContent.find(".tg-ponta-b").hide();
                }

            });

            $(".toggle-diretriz-ponta:eq(0)").trigger("click");

            $(objContent).find("input[type='checkbox']").unbind("click").click(function(){

                objChecked = $(this).prop('checked');
                objValue = $(this).val();

                if (!objChecked) {
                    wUrlAtivIntervDiretriz = baseUrlApp() + "/epmts/intervencoes/atividades/excluidas/registrar/" + objValue;
                    labelAction = "excluída da";
                } else {
                    wUrlAtivIntervDiretriz = baseUrlApp() + "/epmts/intervencoes/atividades/excluidas/remover/" + objValue;
                    labelAction = "adicionada na";
                }

                tlfShowLockWindow();
                $.post(wUrlAtivIntervDiretriz, function(callback){
                    tlfHideLockWindow();
                    if (callback && callback.status == "sucess") {
                        alertLockWindow("A atividade foi " + labelAction + " intervenção");
                    } else {
                        alertLockWindow("A atividade foi " + labelAction + " intervenção");
                    }

                    openDetailsIntervencao(idIntervencao, objDashIntervencoes, positionWindow);

                }, "json");

            });

        });

    });

    $(".interv-pedidos").unbind("click").click(function(){

        idIntervencao = $(this).attr("alt");
        wUrlAtivIntervPedidos = baseUrlApp() + "/epmts/intervencoes/pedidos/" + idIntervencao;

        b = new Boxy(loadingPrefsMsg(), {title: "", modal: true, unloadOnHide: true});
        b.center();

        $.get(wUrlAtivIntervPedidos, function(callback){

            b.setContent(callback);
            formatGrids();
            b.center();

            objContent = b.getContent();

            // toggle pontas
            $(".toggle-pedidos-ponta").unbind("click").click(function(){
                objAlt = $(this).attr("alt");
                if (objAlt == 'b') {
                    objContent.find(".tg-ponta-a").hide();
                    objContent.find(".tg-ponta-b").show();
                } else {
                    objContent.find(".tg-ponta-a").show();
                    objContent.find(".tg-ponta-b").hide();
                }

            });

            $(".toggle-pedidos-ponta:eq(0)").trigger("click");

        });

    });

    $(".inter-ativ-associar").unbind("click").click(function(){

        if (b != undefined) {
            b.hide();
        }

        objAlt = $(this).attr("alt").split(",");

        wUrlAtivInterAdd = baseUrlApp() + "/epmts/intervencoes/dependencias/associar/" + objAlt[0] + "/" + objAlt[1];
        tlfShowLockWindow();
        $(this).genWindowCrudCreate("form[name^='intervencoes_ativs_interdep_']",{
            title: "",
            modal: true
        }, wUrlAtivInterAdd, function(callbackRequest){

            objCon = callbackRequest.window.getContent();
            objCon.find("input[type='text']:eq(0)").focus();

            tlfHideLockWindow();

            $('#sm-intervencao').parent().prev().hide();
            $('#sm-intervencao').parent().next().next().next().next()
                                         .children().first().hide();
            $('small').hide();
        }, function(callbackPost){

            tlfHideLockWindow();

            if (callbackPost.req.status == "sucess") {

                callbackPost.window.hide();
                if (b != undefined) { b.hide(); }

                idIntervBase = $("#detalhes").find("input[name='id_intervencao']").val();

                openDetailsIntervencao(idIntervBase, objDashIntervencoes, positionWindow);

                recarregarInterdependencia(objAlt[1], objArea, node, objAlt[0], objDashIntervencoes, function(){
                    alertLockWindow("A Interdependência foi atualizada com sucesso");
                });

            } else {

                alertLockWindow("Atenção! corrija os erros informados e tente novamente");

            }
        });

        return false;

    });

    $(".remover-ativi-dep").unbind("click").click(function(){

        idAtivDep = $(this).attr("alt").split(",");

        Boxy.confirm("Tem certeza que deseja remover a dependência?", function(){

            tlfShowLockWindow();

            wUrlRemoverAtividadeDependente = baseUrlApp() + "/epmts/intervencoes/dependencias/remover/" + idAtivDep[2];

            $.post(wUrlRemoverAtividadeDependente, {saTableFieldValue: idAtivDep[1]}, function(callback){

                tlfHideLockWindow();

                if (b != undefined) {
                    b.hide();
                }

                openDetailsIntervencao(idIntervencao, objDashIntervencoes, positionWindow);

                recarregarInterdependencia(idAtivDep[1], objArea, node, idAtivDep[0], objDashIntervencoes, function(){
                    alertLockWindow(callback.msg);

                });

            },"json");

        },{title:"Atenção"});

    });

    $(objArea).find(".set-ativ-status").unbind("click").click(function(){

        objThis = this;

        heightSite = $(".site-cronograma-" + node).height();
        idPosHa = $(objThis).closest('form').find("input[name='id_posicao']").val();
        positionWindow = {};
        positionWindow.top = Number(280+(200*idPosHa)+heightSite);

        wUrlAtivRespAdd = baseUrlApp() + "/epmts/intervencoes/registrar/status/" + $(this).attr("alt") + "/" + node;

        $(this).genWindowCrudCreate("form[name^='intervencoes_ativs_status']",{
            title: "",
            modal: true
        }, wUrlAtivRespAdd, function(callbackRequest){

            objCon = callbackRequest.window.getContent();
            objCon.find("input[type='text']:eq(0)").focus();

            $('.labelField:first').parent().prev().hide();
            $('.labelField:first').parent().next().children().first().hide();
        }, function(callbackPost){

            if (callbackPost.req.status == "sucess") {
                alertLockWindow("Um novo status foi definido para a atividade");
                callbackPost.window.hide();
                openDetailsIntervencao(idIntervencao, objDashIntervencoes, positionWindow);
            } else {
                alertLockWindow("Atenção corriga os <B>erros informados</B> e tente novamente");
            }

        });

        return false;

    });

    $(".view-grupos-disp-interv").unbind("click").click(function(){

        checkGroupsVisible();

        objContentGrupos = $("#gruposDisponiveisIntervencao").html();

        b = new Boxy(loadingPrefsMsg(), {title: "", modal: true, unloadOnHide: true});
        b.setContent(objContentGrupos);
        b.center();

        $(".select-all-grupos").unbind("click").click(function(){
            $("input[name^='setGroupView']").each(function(index, element){
                $(element).trigger("click");
            });
        });

        $("input[name^='setGroupView']").each(function(index, element){

            $(element).click(function(){
                objVal = $(element).val();
                changeViewItem(objVal);
                checkGroupsVisible();
            });

        });

    });

    $(".ativ-hist-view").unbind("click").click(function(){

        $('.tipsy').hide();

        objAlt = $(this).attr("alt").split("/");
        objClick = $(this);

        wUrlRespView = baseUrlApp() + "/epmts/intervencoes/atividades/historico/listar/" + objAlt[0] + "/" + objAlt[1];

        b = new Boxy(loadingPrefsMsg(),{title:"", modal:true, unloadOnHide: true});

        $.get(wUrlRespView, function(callback){
            b.setContent(callback);
            formatGrids();
            b.center();
            historicoOptions(objAlt[0], objAlt[1], objClick, b);
        });

    });

    $(".open-interv-ativs-obs").unbind("click").click(function(){
        $('.tipsy').hide();
        id_observacao = $(this).attr("alt");

        positionWindow = $(this).closest('td').offset();

        recarregarObservacoes(id_observacao, objArea, node, idIntervencao, objDashIntervencoes, function(){}, positionWindow);
    });

    $(".reg-ativ-observacao").unbind("click").click(function(){

        id_observacao = $(this).attr("alt");
        wUrlAtivObs = baseUrlApp() + "/epmts/intervencoes/observacoes/registrar/" + id_observacao;

        $(this).genWindowCrudCreate("form[name^='intervencoes_ativs_obs_']",{
            title: "",
            modal: true
        }, wUrlAtivObs, function(callbackRequest){

            objCon = callbackRequest.window.getContent();
            objCon.find("input[type='text']:eq(0)").focus();

            $('.showOptionalFields:first').parent().parent().parent().hide();
            $('#sm-observacao').parent().prev().prev().hide();
            $('#sm-observacao').prev().hide();
            $('#sm-observacao').css('border', '1px solid #00C6D7')
                               .css('border-radius', '4px')
                               .css('width', '100%')
                               .css('padding', '4px');

            $('#sm-observacao').parent().next().children().first().hide();

        }, function(callbackPost){

            heightSite = $(".site-cronograma-" + node).height();
            idPosHa = $('[name="intervencoes_ativs_obs_insert"]').find("input[name='id_posicao']").val();
            positionWindow = {};
            positionWindow.top = Number(280+(200*idPosHa)+heightSite);

            if (callbackPost.req.status == "sucess") {
                $('.showOptionalFields:first').parent().parent().parent().hide();
                $('#sm-observacao').parent().prev().prev().hide();
                $('#sm-observacao').prev().hide();
                $('#sm-observacao').css('border', '1px solid #00C6D7')
                    .css('border-radius', '4px')
                    .css('width', '100%')
                    .css('padding', '4px');

                $('#sm-observacao').parent().next().children().first().hide();

                callbackPost.window.hide();

                b.hide();

                openDetailsIntervencao(idIntervencao, objDashIntervencoes);

                recarregarObservacoes(id_observacao, objArea, node, idIntervencao, objDashIntervencoes, function(){
                    //sMsg = 'Um novo registro foi adicionado à seção';
                    alertLockWindow('Registro adicionado com sucesso');
                }, positionWindow);
            } else {
                $('.showOptionalFields:first').parent().parent().parent().hide();
                $('#sm-observacao').parent().prev().prev().hide();
                $('#sm-observacao').prev().hide();
                $('#sm-observacao').css('border', '1px solid #00C6D7')
                    .css('border-radius', '4px')
                    .css('width', '100%')
                    .css('padding', '4px');

                $('#sm-observacao').parent().next().children().first().hide();
                alertLockWindow("Atenção corrija os <B>erros informados</B> e tente novamente");
            }
        });

        return false;

    });

    $(".interv-resp-view").unbind("click").click(function(){

        $('.tipsy').hide();

        objAlt = $(this).attr("alt");
        objClick = $(this);

        wUrlRespView = baseUrlApp() + "/epmts/intervencoes/responsaveis/" + objAlt;

        b = new Boxy(loadingPrefsMsg(),{title:"", modal:true, unloadOnHide: true});

        $.get(wUrlRespView, function(callback){

            b.setContent(callback);

            formatGrids();
            b.center();

        });

    });

    $(".resp-view").unbind("click").click(function(){

        $('.tipsy').hide();

        objAlt = $(this).attr("alt");
        objClick = $(this);

        wUrlRespView = baseUrlApp() + "/epmts/intervencoes/atividades/responsaveis/" + objAlt;

        b = new Boxy(loadingPrefsMsg(),{title:"", modal:true, unloadOnHide: true});

        $.get(wUrlRespView, function(callback){

            b.setContent(callback);

            formatGrids();
            b.center();

        });

    });

    $(".see-location").unbind("click").click(function(){

        objClick = $(this);
        objValue = $(this).attr("alt").split("|");

        marker = new Array;

        for(i=0; i < objValue.length; i++) {

            arrayGeo = objValue[i].split(",");

            lati = arrayGeo[0];
            longi = arrayGeo[1];

            marker[i] = {
                latitude : lati,
                longitude : longi
            };

        }

        gMapVar = {
            markers : marker,
            maptype: G_SATELLITE_MAP,
            zoom: 16
        };

        $("#maplocation").gMap(gMapVar);
        $("#maplocation").show();

        $("a[href='#map']").trigger("click");

    });

    $(".see-gaant").unbind("click").click(function(){

        idAlt = $(this).attr("alt");

        wUrl = baseUrlApp() + "/epmts/intervencoes/atividades/gaant/" + idAlt;
        $.get(wUrl, function(callback){
            $("#gaant").html(callback);
        });

        $("a[href='#gaant']").trigger("click");

    });

    $(".view-motivo-replan").unbind("click").click(function(){
        console.log("oie");
        idAlt = $(this).attr("alt");
        box = new Boxy(loadingPrefsMsg(),{modal: true, unloadOnHide: true, title: ""});

        wUrlOpenMotivoReplan = baseUrlApp() + "/epmts/intervencoes/atividades/replan/visualizar/" + idAlt;

        $.get(wUrlOpenMotivoReplan, function(callback){

            box.setContent(callback);
            box.center();

            optionsIntervencoes(objArea, node, idIntervencao, objDashIntervencoes, box, positionWindow);

        });

    });

    formatTipsy();

}

$(document).ready(function(){

    if (window.location.hash != "") {

        hash = window.location.hash.replace("#", "");
        arrayData = hash.split(",");
        openDetailsIntervencao(arrayData[1], $("#epmts-dash-intervencoes"));

        $("#epmts-dash-intervencoes ul:eq(0) li a[alt^='" + arrayData[0] + "']").trigger("click");

    } else {

        $("#epmts-dash-intervencoes ul:eq(0) li:eq(0) a").trigger("click");

    }

    processSearchDashIntervencoes();

});

$("#epmts-dash-intervencoes ul").idTabs("tabs2");
//$("#epmts-dash-intervencoes ul li:eq(0) a").trigger("click");
