/**
 * @author José Wilker
 */

(function($){
    $.fn.extend({
        transformForm: function(options) {
			
            var objVars = {
                debug : 0,
                validate : {
                    active : true,
                    titulo : "Campos marcados em amarelo no formulário indicam erro.",
                    rodape : "",
                    type: "box",
                    highlight : true,
                    highlightColor : '#FFFFD4',
                    windowError : "showError",
                    windowSucess : "showConfirm",
                    fields : "text|password",
                    notpermited : ""
                },
                loading: "loading",
                scrollto : "0px",
                reset: false,
                resetChecked: true
            }
			
            var options = $.extend(true, options, objVars);
            var objectFormID = $(this).attr("id");
            var objectForm = $(this);
            var objectFormThis = this;
            var objectWindowError = $("#"+objVars.validate.windowError).attr("id");
			
            // action
            var action = $(this).attr("action");
			
            $(this).submit(function(){
				
                dataSerialize = $(this).serialize();
				
                valButton = $("#" + this.id + " input[type='submit']").val();
				
                $("#" + this.id + " input[type='submit']").val("Aguarde enviando dados...").attr("disabled",true);
				
                if (objVars.validate.active) {
					
                    objVarsValidate = objVars.validate;
					
                    forms = new form();
					
                    $error = "";
					
                    if (objectFormID == "") {
                        $error += "Você precisa configurar o id do formulário";
                    }
					
                    if (objectWindowError == undefined) {
                        $error += "Você precisa adicionar no documento HTML uma tag div com id='" + objVars.validate.windowError + "'";
                    }
					
                    if (action == "") {
                        $error += "Você precisa adicionar um action no seu formulário.";
                    }
					
                    if ($error=="") {
						
                        forms.settings(objVarsValidate.highlightColor,objVarsValidate.highlight,objVarsValidate.type,objVarsValidate.windowError,objVarsValidate.titulo,objVarsValidate.rodape);
						
                        if (forms.camposNulos(objectFormID,objVars.validate.fields,objVars.validate.notpermited)) {
							
                            $("#" + objectFormID + " input[type='submit']").val(valButton).attr("disabled",false);
							
                            $().ajaxStart(function(){
                                $("#"+objVars.loading).show();
                            });
							
                            $.post(action,dataSerialize,function(data){
									
                                switch(data.type) {
                                    case "error":
											
                                        obWE = objVars.validate.windowError;
                                        $("#" + obWE).html(data.text);
                                        $("#" + obWE).show();
										
                                        break;
                                    case "sucess":
											
                                        obWS = objVars.validate.windowSucess;
											
                                        $("#" + obWS).html(data.text);
                                        $("#" + obWS).show();
											
                                        if (data.reset) {
                                            $(objectForm)[0].reset();
                                        }
											
                                        if (data.resetChecked) {
                                            $("#" + objectFormID + " input:checked").each(function(){
                                                $(this).attr("checked",false);
                                            });
                                        }
											
                                        if (data.clearDivId) {
												
                                            $(data.clearDivId).each(function(){
                                                $(this).hide();
                                            });
                                        }
											
                                        break;
                                    case "location":
                                        window.location = data.url;
                                        break;
                                }
									
                                $.scrollTo(objVars.scrollto,300);
									
                            }, "json");
							
                        } else {
							
                            $("#" + objectFormID + " input[type='submit']").val(valButton).attr("disabled",false);
							
                            $.scrollTo(objVars.scrollto,300);
                            return false;
                        }
							
                    } else {
						
                        $("#" + objectFormID + " input[type='submit']").val(valButton).attr("disabled",false);
						
                        alert($error);
                    }
					
                } else {
                    $.post(action,dataSerialize,function(data){
						
                        if (data.type == "error") {
                            $("#" + objVars.validate.windowError).html(data.text);
                            $("#" + objVars.validate.windowError).show();
                        }
									
                        if (data.type == "sucess") {
                            $("#" + objVars.validate.windowSucess).html(data.text);
                            $("#" + objVars.validate.windowSucess).show();
                        }
						
                        $("#" + objectFormID + " input[type='submit']").val(valButton).attr("disabled",false);
						
                    },"json");
                }
				
                return false;
            //send();
            })
        },
        transformFormTabs : function(opts){
			
            options = {
                color : {
                    active : "#dddddd",
                    inative : "#F3F3F3"
                }
            }
            opts = $.extend(options,opts);
            objAba = $(this).attr(opts.tabs.attr);
            objAreaAbas = $(this);
			
            if (opts.form != undefined) {
                $(opts.form.objeto).find(opts.tabs.matchContent + "[" + opts.tabs.attr + "^='" + opts.tabs.prefix + "-']").each(function(index,element){
                    if ($(this).attr("alt") != "onload") {
                        $(this).hide();
                    } else {
                        className = $(this).attr("class").split("-");
                        if (className[1].match(/\S/)) {
                            tabSelected = objAreaAbas.find(opts.tabs.matchLine + "[alt='"+className[1]+"']");
                            _setActiveTab(tabSelected,opts);
                        }
                    }
                });
            }
			
            $(this).find("li").click(function(){
				
                _desactiveAllTabs(objAreaAbas,this,opts);
                _showContentTab(opts,this);
				
            });
			
            /* Tab Helpers */
			
            // helper para deixar uma aba no estado ativo.
            function _setActiveTab(tabSelected,opts) {
                $(tabSelected).css({
                    "background-color":opts.color.active
                    });
            }
			
            // helper para deixar uma aba no estado inativo.
            function _setInactiveTab(objAreaAbas,opts) {
                objAreaAbas.find("li").each(function(){
                    $(this).css({
                        "background-color":opts.color.inative
                        });
                });
            }
			
            // helper para exibir o conteúdo de uma tab.
            function _showContentTab(opts,t) {
                $(opts.form.objeto).find(opts.tabs.matchContent + "[class^='" + opts.tabs.prefix + "-" + $(t).attr("alt") + "']").show();
            }
			
            // helper para desativar todas as abas que estiverem ativas.
            function _desactiveAllTabs(objAreaAbas,tabSelected,opts) {
                _setInactiveTab(objAreaAbas,opts);
                _setActiveTab(tabSelected,opts);
                $(opts.form.objeto).find(opts.tabs.matchContent + "[class^='" + opts.tabs.prefix + "-']").hide();
				
            }
			
        },
        transformMinimalForm: function(opts){
			
            options = {
                input : "", // "td",
                actionInsert : "", // "http://10.211.55.3/tal",
                actionDelete : "", // "http://10.211.55.3/tal",
                id : "", // "tID",
                value : "", // "tInput",
                msg : {
                    confirm : "tem certeza que deseja confirmar as alterações",
                    nothing : "..."
                },
                mask :{
                    0 : {
                        field : "telefone",
                        rule : "(99) 9999-9999"
                    }
                },
                tipsy : {
                    gravity : "s",
                    title : "Apagar contato"
                },
                css : {},
                data : {
                    type : "null",
                    objs : {
                        idReturn : "id_contato",
                        idAttr : "id"
                    },
                    text : {
                        mensagem : {
                            sucess : "",
                            error : ""
                        }
                    }
                }
            };
			
            opts = $.extend(options,opts);
			
            obj = this;
			
            $(this).find(opts.input).parent().each(function(index,element){
				
                blockDelete = document.createElement('div');
                $(blockDelete).css({
                    "background-color" : "black",
                    "font-size" : "10px",
                    "color" : "white",
                    "position" : "absolute",
                    "padding" : "2px",
                    "padding-top" : "0px",
                    "width" : "5px",
                    "height" : "12px",
                    "margin-left" : "-15px",
                    "margin-top" : "-7px",
                    "-moz-border-radius":"2px",
                    "-webkit-border-radius":"2px"
                }).text('x').hover(function(){
                    $(this).css({
                        "background-color" : "#c3c3c3",
                        "color" : "#4b4b4b",
                        "cursor" : "pointer"
                    })
                },function(){
                    $(this).css({
                        "background-color" : "black",
                        "color":"white",
                        "cursor" : "pointer"
                    });
                }).attr({
                    "id" : "SAtmf-block-delete"
                });
				
                if (opts.tipsy) {
                    $(blockDelete).attr({
                        "title":opts.tipsy.title
                        }).tipsy({
                        gravity: opts.tipsy.gravity
                        });
                }
                $(this).find("td:first-child").html(blockDelete);
                if ($(this).attr("id") == "null") {
                    $(blockDelete).hide();
                }
				
                $(blockDelete).click(function(){
                    objActiveDeleteBlock = $(this);
                    msgConfirm = "Tem certeza que deseja apagar este registro?";
					
                    if (confirm(msgConfirm)) {
                        objClickLine = $(this).parent().parent();
                        idLine = objClickLine.attr("id");
                        wUrlDelete = opts.actionDelete;
                        $.post(wUrlDelete,{
                            id:idLine
                        },function(data){
							
                            trs = objClickLine.parent().find("tr").length;
							
                            if (data.line != undefined) {
                                if (data.line.remove) {
                                    if (trs >= 2) {
                                        alert("Registro apagado com sucesso, essa página será atualizada.");
                                        window.parent.location.reload();
                                    } else {
                                        objClickLine.attr({
                                            "id":"null"
                                        });
                                        objClickLine.find("td").each(function(index,element){
                                            $(element).text("...");
                                        });
                                        objClickLine.find("td:first-child").text("");
                                    }
                                }
                            }
                        },"json");
                    }
					
                });
				
            });
        }
    });
})(jQuery);
