function form_add_new_user(objForm, b) {

    $(objForm).find("input[name='definir_senha']").click(function(){

        checked = $(this).is(':checked');
        if (checked) {
            $(objForm).find("input[type='password']").removeAttr("disabled");
        } else {
            $(objForm).find("input[type='password']").attr({"disabled":"disabled"});
        }

    });
    $(objForm).find("input[type='text']:eq(0)").focus();
    $(objForm).find("input[type='password']").attr({"disabled":"disabled"});

    $(objForm).find(".btnConfirmCreate").click(function(){
        $(objForm).trigger("submit");
    });

    $(objForm).submit(function(){

        actionButton = $(objForm).find("input[type='submit']");
        actionButton.attr({
            "disabled":"disabled"
        }).val("Aguarde ... ");

        objSerialize = $(this).serialize();
        wUrlCadastro = $("#sa-baseurl").val() + "usuarios/cadastro";

        $.post(wUrlCadastro, objSerialize, function(callbackCadastro){

            if (callbackCadastro.type == "error") {

                $.jGrowl(callbackCadastro.msg,{
                    header: "Atenção"
                });
                actionButton.removeAttr("disabled").val("Criar usuário");

            } else {
                $.jGrowl(callbackCadastro.msg,{
                    header:"Atenção"
                });
                b.setContent(callbackCadastro.content);
                b.center();
                $(".users-menu").find("li:eq(0)").trigger("click");
            }

        }, "json");

        return false;

    });

}

function _sa_addNewUser() {
    $(".add-new-user").unbind("click").click(function(){

        wUrl = $("#sa-baseurl").val() + "usuarios/cadastro";

        boxy = new Boxy("<div>Aguarde ... carregando...</div>", {
            title: "&nbsp;",
            modal: true,
            unloadOnHide: true
        });

        $.getJSON(wUrl, function(callback){

            boxy.setContent("<div>" + callback.content + "</div>");

            boxy.center();
            objContentBoxy = boxy.getContent();

            objForm = objContentBoxy.find("form[name='form-users']");

            form_add_new_user(objForm, boxy);
        });
    });
}

$(document).ready(function(){

    _sa_addNewUser();

    $(".users-menu").find("li").click(function(){

        objOption = $(this).attr("id");
        wUrl = $("#sa-baseurl").val() + "usuarios";

        $(".users-menu").find("li").removeClass("selected");

        switch(objOption) {
            case "desenvolvedores":
                wUrl += "/developers";
                break;
            case "list-users":
                wUrl += "/listar/cadastrados";
                break;
            case "list-blocked":
                wUrl += "/listar/bloqueados";
                break;
            case "list-apps":
                wUrl += "/permissoes";
                break;
            case "start-presentation":
                wUrl += "/apresentacao";
                break;
        }

        window.location.hash = objOption;

        $(this).addClass("selected");

        $.getJSON(wUrl, function(callback){

            $(".users-content").html(callback.content);

            $(".users-list").find("li").hover(function(){
                $(this).find(".users-bar-options-list").show();
            }, function(){
                $(".users-content").find(".users-list").find("li").find(".users-bar-options-list").hide();
            });

            switch(objOption) {
                case "desenvolvedores":
                    $(".users-content").find(".delete-developer").click(function(){
                        objIdDeveloper = $(this).attr("alt");
                        Boxy.ask("Tem certeza que deseja remover a credencial de desenvolvedor do usuário?", ["Confirmar", "Cancelar"], function(val) {
                            switch(val) {
                                case "Confirmar":
                                    wUrlDeleteDesenvolvedor = $("#sa-baseurl").val() + "usuarios/developers/remove/" + objIdDeveloper;
                                    $.post(wUrlDeleteDesenvolvedor,{}, function(callback){
                                        if (callback.type == "sucess") {
                                            $.jGrowl(callback.msg,{
                                                header:"Atenção"
                                            });
                                            $(".users-menu").find("li:eq(1)").trigger("click");
                                        } else {
                                            $.jGrowl(callback.msg,{
                                                header:"Atenção"
                                            });
                                        }
                                    },"json");

                                    break;
                            }
                        }, {
                            title: "Confirme o que deseja fazer."
                        });
                    });
                break;
                case "list-users": case "list-blocked":

                    $(".users-content").find(".unblock-user").click(function(){
                        objIdUserBloqueado = $(this).attr("alt");
                        Boxy.ask("Tem certeza que deseja desbloquear o acesso do usuário?", ["Confirmar", "Cancelar"], function(val) {
                            switch(val) {
                                case "Confirmar":
                                    wUrlUnblockUser = $("#sa-baseurl").val() + "usuarios/bloqueados/remover/" + objIdUserBloqueado;
                                    $.post(wUrlUnblockUser,{}, function(callback){
                                        if (callback.type == "sucess") {
                                            $.jGrowl(callback.msg,{
                                                header:"Atenção"
                                            });
                                            $(".users-menu").find("li:eq(2)").trigger("click");
                                        } else {
                                            $.jGrowl(callback.msg,{
                                                header:"Atenção"
                                            });
                                        }
                                    },"json");

                                    break;
                            }
                        }, {
                            title: "Confirme o que deseja fazer."
                        });
                    });

                    $(".users-content").find(".edit-user").click(function(){
                        objAssociacao = $(this).attr("alt");
                        wUrlEdit = $("#sa-baseurl").val() + "usuarios/alterar/cadastro/" + objAssociacao;

                        $.get(wUrlEdit, function(callbackEdit){

                            boxyAreaEdit = new Boxy(callbackEdit, {title: "Alterar informações", unloadOnHide: true, modal: true});

                            objForm = $("form[name='form-users']");

                            $(objForm).find(".btnConfirm").click(function(){
                                $(objForm).trigger("submit");
                            });

                            objForm.submit(function(){
                                objSerialize = $(this).serialize();

                                $.post(wUrlEdit, objSerialize, function(callbackPostEdit){

                                    if (callbackPostEdit.type == "error") {
                                        $.jGrowl(callbackPostEdit.msg,{
                                            header:"Atenção"
                                        });
                                    } else {
                                        $.jGrowl(callbackPostEdit.msg,{
                                            header:"Atenção"
                                        });
                                        boxyAreaEdit.hide();
                                        $(".users-menu").find("li:eq(0)").trigger("click");
                                    }

                                }, "json");
                            });

                        });
                    });

                    $(".users-content").find(".block-user, .delete-user").each(function(){
                        $(this).click(function(){
                            objAssoc = $(this).attr("alt");
                            objClassName = $(this).attr("class");
                            if (objClassName == "delete-user") {
                                objClassText = "apagar";
                                wUrlDel = $("#sa-baseurl").val() + "usuarios/apagar";
                            } else {
                                objClassText = "bloquear";
                                wUrlDel = $("#sa-baseurl").val() + "usuarios/bloquear";
                            }
                            Boxy.ask("Tem certeza que deseja <b>" + objClassText + "</b> a conta do usuário?", ["Confirmar", "Cancelar"], function(val) {
                                switch(val) {
                                    case "Confirmar":

                                        $.post(wUrlDel,{
                                            id_associacao: objAssoc
                                        }, function(callback){
                                            if (callback.type == "sucess") {
                                                $.jGrowl(callback.msg,{
                                                    header:"Atenção"
                                                });
                                                $(".users-menu").find("li:eq(0)").trigger("click");
                                            } else {
                                                $.jGrowl(callback.msg,{
                                                    header:"Atenção"
                                                });
                                            }
                                        },"json");

                                        break;
                                }
                            }, {
                                title: "Confirme o que deseja fazer."
                            });
                        });
                    });

                    $(".users-content").find(".registrar-desenvolvedor").click(function(){

                        usuario = $(this).attr("alt");
                        wUrlDeveloper = $("#sa-baseurl").val() + "usuarios/developers/registrar";

                        Boxy.confirm("Tem certeza que deseja registrar o usuário como <b>desenvolvedor</b>.", function(){
                            $.post(wUrlDeveloper, {usuario: usuario}, function(callbackPost){
                                if (callbackPost.type == "sucess") {
                                    $.jGrowl(callbackPost.msg,{
                                        header:"Atenção"
                                    });
                                    $(".users-menu").find("li:eq(0)").trigger("click");
                                } else {
                                    $.jGrowl(callbackPost.msg,{
                                        header:"Atenção"
                                    });
                                }
                            }, "json");
                        }, {title: "Atenção"});

                    });

                break;
                case "list-apps":
                    $(".usuarios-acesso").click(function(){
                        form_acesso_usuarios(this);
                    });
                break;
            }
        });

    });

    if (window.location.hash != "") {
        hash = window.location.hash.replace("#", "");
        $(".users-menu").find("#" + hash).trigger("click");
    } else {
        $(".users-menu").find("li:first-child").trigger("click");
    }

});